import Darwin

func GaussianKernel(width: Int, height: Int, sigma: Double) -> [[Double]] {
	var sum = 0.0 // Needed for nomalization later
	
	var kernel = (0..<height).map { row -> [Double] in
		(0..<width).map { col -> Double in
			let x = col - width / 2
			let y = row - height / 2
			
			let value = exp(-(Double(x*x + y*y) / (2 * sigma * sigma)))
			
			sum += value
			
			return value
		}
	}
	
	//Normalize
	for y in (0..<height) {
		for x in (0..<width) {
			kernel[x][y] /= sum
		}
	}
	
	return kernel
}

var kernel = GaussianKernel(width: 5, height: 5, sigma: 1);

for row in kernel {
	for element in row {
		print("\(element), ", terminator: "")
	}
	print()
}
