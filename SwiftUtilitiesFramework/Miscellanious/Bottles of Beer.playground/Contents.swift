func bottlePhrase(for count: Int) -> String {
	switch count {
	case 0: return "no more bottles of "
	case 1: return "1 bottle"
	case _: return "\(count) bottles"
	}
}

func beerSong(bottleCount: Int) -> String {
	let bottlePhrases = (0...bottleCount)
		.lazy
		.reversed()
		.map{ bottlePhrase(for: $0) + " of beer" }
	
	let mainBody = zip(bottlePhrases, bottlePhrases.dropFirst())
		.map { bottlePhrase, oneLessBottlePhrase in return """
			\(bottlePhrase) on the wall, \(bottlePhrase).
			Take one down and pass it around, \(oneLessBottlePhrase) on the wall.
			
			"""
		}
		.joined(separator: "\n")
	
	return mainBody + """
	\nNo more bottles of beer on the wall, no more bottles of beer.
	Go to the store and buy some more, \(bottleCount) bottles of beer on the wall.
	"""
}

print(beerSong(bottleCount: 5))
