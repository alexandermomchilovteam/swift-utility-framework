//
//  Quadratic Equation.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public enum QuadraticRoots<T: Numeric> {
	case two(T, T)
	case one(T)
	case none
}

public extension QuadraticRoots {
	func applyWithSelfOnLeft(_ fn: (T, T) -> T, withOperand value: T) -> QuadraticRoots {
		switch self {
		case let .two(a, b): return .two(fn(a, value), fn(b, value))
		case let .one(a): return .one(fn(a, value))
		case .none: return .none
		}
	}
	
	func applyWithSelfOnRight(withOperand value: T, _ fn: (T, T) -> T) -> QuadraticRoots {
		switch self {
		case let .two(a, b): return .two(fn(value, a), fn(value, b))
		case let .one(a): return .one(fn(value, a))
		case .none: return .none
		}
	}
	
	static func discriminantOf(xSquared a: T, x b: T, c: T) -> T { return b*b - 4*a*c }
}

public extension QuadraticRoots where T: FloatingPoint {
	static func solveFrom(xSquared a: T, x b: T, c: T) -> QuadraticRoots {
		let discriminant = self.discriminantOf(xSquared: a, x: b, c: c)
		return (-b ± sqrt(discriminant)) / (2 * a)
	}
}

infix operator ± : AdditionPrecedence
public func ± <T: Numeric>(left: T, right: T) -> QuadraticRoots<T> {
	return QuadraticRoots.two(left + right, left - right)
}
