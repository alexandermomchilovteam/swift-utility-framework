//
//  BitVector.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//


struct BitVector {
	enum Varient {
		case large(BitVectorGuts)
		case small(UInt32)
	}

	init() {

	}
}
