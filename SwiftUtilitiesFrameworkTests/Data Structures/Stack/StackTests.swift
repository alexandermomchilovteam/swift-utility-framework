//
//  StackTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-06-10.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import SwiftUtilitiesFramework

class StackTests: XCTestCase {
	func testQueue() {
		var s = Stack<Int>()

		XCTAssertNil(s.pop())

		s.push(1)
		XCTAssertEqual(s.pop(), 1)
		XCTAssertNil(s.pop())

		s.push(1)
		s.push(2)
		XCTAssertEqual(s.pop(), 2)
		XCTAssertEqual(s.pop(), 1)
		XCTAssertNil(s.pop())


		s.push(1)
		s.push(2)
		s.push(3)
		XCTAssertEqual(s.pop(), 3)
		XCTAssertEqual(s.pop(), 2)
		XCTAssertEqual(s.pop(), 1)
		XCTAssertNil(s.pop())
	}
}
