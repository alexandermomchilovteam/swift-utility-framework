//
//  Quadratic Roots Operators.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public extension QuadraticRoots where T: Numeric {
	static func + (quadRoots: QuadraticRoots, rhs: T) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnLeft(+, withOperand: rhs)
	}
	static func - (quadRoots: QuadraticRoots, rhs: T) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnLeft(-, withOperand: rhs)
	}
	static func * (quadRoots: QuadraticRoots, rhs: T) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnLeft(*, withOperand: rhs)
	}
	
	
	static func + (lhs: T, quadRoots: QuadraticRoots) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnRight(withOperand: lhs, +)
	}
	static func - (lhs: T, quadRoots: QuadraticRoots) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnRight(withOperand: lhs, -)
	}
	static func * (lhs: T, quadRoots: QuadraticRoots) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnRight(withOperand: lhs, *)
	}
}

public extension QuadraticRoots where T: FloatingPoint {
	static func / (quadRoots: QuadraticRoots, rhs: T) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnLeft(/, withOperand: rhs)
	}
	
	static func / (lhs: T, quadRoots: QuadraticRoots) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnRight(withOperand: lhs, /)
	}
}

public extension QuadraticRoots where T: BinaryFloatingPoint { // For integer (truncating) division
	static func / (quadRoots: QuadraticRoots, rhs: T) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnLeft(/, withOperand: rhs)
	}
	
	static func / (lhs: T, quadRoots: QuadraticRoots) -> QuadraticRoots {
		return quadRoots.applyWithSelfOnRight(withOperand: lhs, /)
	}
}
