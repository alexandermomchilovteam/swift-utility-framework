import SwiftUtilitiesFramework

let array = Array(0..<5)

for i in -10 ... 10 {
	let a = array << i
	var b = array
	b <<= i
	
	let c = array >> i
	var d = array
	d >>= i
	
	print(a, b, c, d)
}


extension Array {
	public mutating func shiftLeftInPlace(by rawOffset: Int = 1) {
		if rawOffset == 0 { return /* no-op */ }
		
		func shiftedIndex(for index: Int) -> Int {
			let attemptedIndex = index + rawOffset
			
			if attemptedIndex < 0 {
				return attemptedIndex % self.count + self.count
			}
			
			return attemptedIndex % self.count
		}
		
		// Create a sequence of indexs of items that need to be swapped.
		// For example, to shift ["A", "B", "C", "D", "E"] left by 1:
		// Swap
		let indexes = sequence(first: 0, next: { index in
			let nextIndex = shiftedIndex(for: index)
			if nextIndex == 0 { return nil } // We've come full-circle
			return nextIndex
		})
		
		print(self)
		for (source, dest) in zip(indexes.dropFirst(), indexes) {
			self.swapAt(source, dest)
			print("Swapping \(source) with \(dest): \(self)")
		}
		print(Array<(Int, Int)>(zip(indexes.dropFirst(), indexes)))
	}
}

var a = ["A", "B", "C", "D", "E"]
print(a)
a.shiftLeftInPlace(by: 2)
print(a)
