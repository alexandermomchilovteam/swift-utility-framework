import SwiftUtilitiesFramework

let testSSNString = "123-45-678"
let optionalTestSSN = SSN(testSSNString)

guard let testSSN = optionalTestSSN else {
	assertionFailure("Valid SSN (\(testSSNString)) wasn't successfully parsed.")
	fatalError()
}

assert(testSSN.description == testSSNString, "SSN (\(testSSN)) was not correctly converted back to String.")

func assertSSNShouldBeNil(_ string: String, because reason: String) {
	assert(SSN(string) == nil, reason + " should be nil.")
}


assertSSNShouldBeNil("123-45-678-9",	because: "SSN with too many segment")
assertSSNShouldBeNil("123-45", 			because: "SSN with too few segments")
assertSSNShouldBeNil("",				because: "Empty SSN")


assertSSNShouldBeNil("000-12-345",		because: "SSN with all-zero segment 1")
assertSSNShouldBeNil("123-00-456",		because: "SSN with all-zero segment 2")
assertSSNShouldBeNil("123-45-000",		because: "SSN with all-zero segment 3")

assertSSNShouldBeNil("666-12-345",		because: "SSN starting with 666")
assertSSNShouldBeNil("900-12-345",		because: "SSN starting with number in range 900-999")
assertSSNShouldBeNil("999-12-345",		because: "SSN starting with number in range 900-999")
