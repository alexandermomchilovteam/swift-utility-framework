//
//  RunLengthDecoding.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public extension Sequence {
	func runLengthDecoded<E: Equatable>() -> LazySequenceRunLengthDecoder<Self, E>
		where Self.Element == (element: E, count: Int) {
			return LazySequenceRunLengthDecoder(decoding: self)
	}
}

public struct LazySequenceRunLengthDecoder<WrappedSequence: Sequence, Value: Equatable>: Sequence
where WrappedSequence.Iterator.Element == (element: Value, count: Int) {
	public let wrappedSequence: WrappedSequence
	
	public init(decoding wrappedSequence: WrappedSequence) {
		self.wrappedSequence = wrappedSequence
	}
	
	public func makeIterator() -> RunLengthDecodingIterator<WrappedSequence.Iterator, Value> {
		return RunLengthDecodingIterator(encoding: wrappedSequence.makeIterator())
	}
}

public struct RunLengthDecodingIterator<WrappedIterator: IteratorProtocol, Value>: IteratorProtocol
where WrappedIterator.Element == (element: Value, count: Int) {
	
	public private(set) var wrappedIterator: WrappedIterator
	public private(set) var currentGrouping: (element: Value, count: Int)? = nil
	
	public init(encoding wrappedIterator: WrappedIterator) {
		self.wrappedIterator = wrappedIterator
	}
	
	public mutating func next() -> Value? {
		let currentGrouping: (element: Value, count: Int)
		
		if let existingGrouping = self.currentGrouping, 0 < existingGrouping.count {
			currentGrouping = existingGrouping
		} else { // First run, or finished the last run
			guard let newGrouping = wrappedIterator.next() else { return nil } // Get the next run
			currentGrouping = newGrouping
		}
		
		self.currentGrouping = (element: currentGrouping.element, count: currentGrouping.count - 1)
		return currentGrouping.element
	}
}
