//
//  Least Common Multiple.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public func lcm<Number: BinaryInteger>(lhs: Number, _ rhs: Number) -> Number {
	return lhs * rhs / gcd(lhs, rhs)
}
