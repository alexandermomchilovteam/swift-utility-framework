//
//  UInt64 bit indexing.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension UInt64 {

	// MSB is bit index 63, LSB is bit index 0
	static func createMask(withBitAtIndex bitIndex: Int) -> UInt64 {
		return 1 << UInt64(bitIndex)
	}

	mutating func setBit(atIndex bitIndex: Int) {
		self |= UInt64.createMask(withBitAtIndex: bitIndex)
	}

	mutating func clearBit(atIndex bitIndex: Int) {
		self &= ~UInt64.createMask(withBitAtIndex: bitIndex)
	}

	subscript(bitIndex bitIndex: Int) -> Bool {
		get {
			let mask = UInt64.createMask(withBitAtIndex: bitIndex)
			return (self & mask) == mask
		}
		set {
			if newValue { self.setBit(atIndex: bitIndex) }
			else { self.clearBit(atIndex: bitIndex) }
		}
	}
}
