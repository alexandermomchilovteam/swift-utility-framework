//
//  QueueTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-06-10.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import SwiftUtilitiesFramework

class QueueTests: XCTestCase {
	func testQueue() {
		var q = Queue<Int>()

		XCTAssertNil(q.dequeue())

		q.enqueue(1)
		XCTAssertEqual(q.dequeue(), 1)
		XCTAssertNil(q.dequeue())

		q.enqueue(1)
		q.enqueue(2)
		XCTAssertEqual(q.dequeue(), 1)
		XCTAssertEqual(q.dequeue(), 2)
		XCTAssertNil(q.dequeue())


		q.enqueue(1)
		q.enqueue(2)
		q.enqueue(3)
		XCTAssertEqual(q.dequeue(), 1)
		XCTAssertEqual(q.dequeue(), 2)
		XCTAssertEqual(q.dequeue(), 3)
		XCTAssertNil(q.dequeue())
	}
}
