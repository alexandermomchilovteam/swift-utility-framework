//
//  OnBitCount.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public extension BinaryInteger {
	var nonzeroBitCount: Int {
		var count = 0
		var i = self
		
		while i != 0 {
			if i & 1 != 0 { count += 1 }
			i >>= 1
		}
		
		return count
	}
	
	var zeroBitCount: Int { return self.bitWidth - self.nonzeroBitCount }
}
