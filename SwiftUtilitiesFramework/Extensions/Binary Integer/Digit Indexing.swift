//
//  Digit Indexing.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public extension BinaryInteger {
	subscript(index: Self) -> Self {
		return self[index, 10]
	}
	
	subscript(index: Self, base: Self) -> Self {
		let temp = base ** index
		return self % (temp * base) / temp
	}
}
