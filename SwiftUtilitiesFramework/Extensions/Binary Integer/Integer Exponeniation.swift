//
//  Integer Exponeniation.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//
infix operator **

public extension BinaryInteger {
	
	private func exponentiate(exponent: Self) -> Self {
		guard (0 as Self) <= exponent else {
			fatalError("Can't have a negative exponent.")
		}
		var base = self
		var exponent = exponent
		var result = 1 as Self
		
		while exponent != 0 {
			if exponent & 1 != 0 { result *= base }
			exponent /= 2
			base *= base
		}
		
		return result
	}
	
	static func **(lhs: Self, rhs: Self) -> Self {
		return lhs.exponentiate(exponent: rhs)
	}
}
