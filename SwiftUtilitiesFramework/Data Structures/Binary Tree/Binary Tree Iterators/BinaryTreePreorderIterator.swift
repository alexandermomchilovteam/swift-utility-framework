//
//  BinaryTreePreorderIterator.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-30.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//


public extension BinaryTreeNodeProtocol {
	var preorderTraversal: BinaryTreePreorderSequence<Self> { return BinaryTreePreorderSequence(self) }
}

public struct BinaryTreePreorderSequence<Tree: BinaryTreeNodeProtocol>: Sequence {
	private let tree: Tree
	
	public init(_ tree: Tree) { self.tree = tree }
	
	public func makeIterator() -> BinaryPreorderIterator<Tree> {
		return BinaryPreorderIterator(tree: self.tree)
	}
}

public struct BinaryPreorderIterator<Tree: BinaryTreeNodeProtocol>: IteratorProtocol {
	private var backtrackStack: [Tree]
	
	public init(tree: Tree) {
		backtrackStack = [tree]
	}
	
	public mutating func next() -> Tree.Payload? {
		guard let node = backtrackStack.popLast() else {
			return nil // Stack is empty.
		}
		if let rightChild = node.right { // Appending right first means it's visit *later*
			backtrackStack.append(rightChild)
		}
		if let leftChild = node.left {
			backtrackStack.append(leftChild)
		}
		
		return node.payload
	}
}
