//
//  Collection Batching.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-05.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension Collection {
	public func batch(size: Int) -> BatchingCollection<Self> {
		return BatchingCollection(wrapping: self, batchSize: size)
	}
}

fileprivate extension Collection {
	func makeBatchStartIndex(batchIndex: Int, batchSize: Int) -> Index {
		var startIndex = self.startIndex
		_ = self.formIndex(
			&startIndex,
			offsetBy: (batchIndex * batchSize),
			limitedBy: self.endIndex
		)
		return startIndex
	}
	
	func makeBatchIndices(start startIndex: Index, batchSize: Int) -> Range<Index> {
		var endIndex = startIndex
		_ = self.formIndex(
			&endIndex,
			offsetBy: +batchSize,
			limitedBy: self.endIndex
		)
		return startIndex ..< endIndex
	}
}
extension ClosedRange {
	func clamp(_ value : Bound) -> Bound {
		return self.lowerBound > value ? self.lowerBound
			: self.upperBound < value ? self.upperBound
			: value
	}
}

public struct BatchingCollection<WrappedCollection: Collection>: BidirectionalCollection {
	public typealias Index = Int
	public typealias Element = WrappedCollection.SubSequence
	
	let wrappedCollection: WrappedCollection
	let batchSize: Int
	
	init(wrapping wrappedCollection: WrappedCollection, batchSize: Int) {
		self.wrappedCollection = wrappedCollection
		self.batchSize = batchSize
	}
	
	public var count: Int {
		// Make Int division round up rather than truncate down
		return ((wrappedCollection.count - 1) / batchSize) + 1
	}
	
	public var startIndex: Index { return 0 }
	public var endIndex: Index { return self.count }
	
	public func index( after i: Index) -> Index { return i + 1 }
	public func index(before i: Index) -> Index { return i - 1 }
	public func index(     _ i: Index, offsetBy offset: Int) -> Index { return i + offset }
	public func formIndex(_ i: inout Index, offsetBy offset: Int, limitedBy limit: Index) {
		i = (self.startIndex...limit).clamp(self.index(i, offsetBy: offset))
	}
	
	public subscript(index: Index) -> Element {
		get {
			let start = wrappedCollection.makeBatchStartIndex(
				batchIndex: index,
				batchSize: batchSize
			)
			let indices = wrappedCollection.makeBatchIndices(
				start: start,
				batchSize: batchSize
			)
			return wrappedCollection[indices]
		}
	}
	
	public func makeIterator() -> BatchingCollectionIterator<WrappedCollection> {
		return BatchingCollectionIterator(wrapping: self.wrappedCollection, batchSize: batchSize)
	}
}

public struct BatchingCollectionIterator<WrappedCollection: Collection>: IteratorProtocol {
	private let wrappedCollection: WrappedCollection
	private let batchSize: Int
	private var progress: WrappedCollection.Index
	
	public init(wrapping wrappedCollection: WrappedCollection, batchSize: Int) {
		self.wrappedCollection = wrappedCollection
		self.batchSize = batchSize
		self.progress = wrappedCollection.startIndex
	}
	
	mutating public func next() -> WrappedCollection.SubSequence? {
		let indices = wrappedCollection.makeBatchIndices(
			start: progress,
			batchSize: batchSize
		)
		
		if indices.isEmpty { return nil }
		
		progress = indices.upperBound
		return wrappedCollection[indices]
	}
}
