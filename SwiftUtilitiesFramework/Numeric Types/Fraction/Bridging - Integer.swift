//
//  Bridging - Integer.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension Fraction {
	init(_ i: Int) {
		self.init(numerator: i, denominator: 1)
	}
}

infix operator ÷: MultiplicationPrecedence

public func ÷ (lhs: Int, rhs: Int) -> Fraction {
	return Fraction(numerator: lhs, denominator: rhs)
}
