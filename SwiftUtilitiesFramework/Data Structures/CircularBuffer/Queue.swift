//
//  Queue.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-28.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public struct Queue<Element> {
	private var elements = CircularBuffer<Element>()
	
	public init() {
		self.elements = CircularBuffer()
		
	}
	
	public init<S: Sequence>(elements: S) where S.Element == Element {
		self.elements = CircularBuffer(elements)
	}
	
	public init(initialCapacity: Int) {
		self.elements = CircularBuffer(initialCapacity: initialCapacity)
	}
	
	public init(reapating element: Element, count: Int) {
		self.elements = CircularBuffer(repeating: element, count: count)
	}
	
	
	public func peek() -> Element? {
		return elements.first
	}
	
	@discardableResult
	public mutating func dequeue() -> Element? {
		return elements.popFirst()
	}
	
	public mutating func enqueue(_ newElement: Element) {
		return elements.append(newElement)
	}
	
}

extension Queue: ExpressibleByArrayLiteral {
	public init(arrayLiteral elements: Element...) {
		self.init(elements: elements)
	}
}
