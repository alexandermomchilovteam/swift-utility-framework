import SwiftUtilitiesFramework

extension BinaryInteger {
	var binaryDescription: String {
		var binaryString = ""
		var internalNumber = self
		var counter = 0
		
		for _ in (1...self.bitWidth) {
			binaryString.insert(contentsOf: "\(internalNumber & 1)", at: binaryString.startIndex)
			internalNumber >>= 1
			counter += 1
			if counter % 4 == 0 {
				binaryString.insert(contentsOf: " ", at: binaryString.startIndex)
			}
		}
		
		return binaryString
	}
}

extension AnyPackedFraction {
	func test() {
		print("\(self.i.binaryDescription) with \(qFormatDescription) encoding is: \(numerator.binaryDescription) (numerator: \(numerator)) / \(denominator.binaryDescription) (denominator: \(denominator)) = \(ratio)")
	}
}

extension PackedFraction {
	func test() {
		print("\(self.i.binaryDescription) with \(Self.qFormatDescription) encoding is: \(numerator.binaryDescription) (numerator: \(numerator)) / \(denominator.binaryDescription) (denominator: \(denominator)) = \(ratio)")
	}
}

PackedFraction16_0(bitPattern: 0b0011_1110___0000_1111).test()
PackedFraction8_8(bitPattern: 0b0011_1110___0000_1111).test()
PackedFraction4_12(bitPattern: 0b0011_1110___0000_1111).test()

//                     ↙︎ This common "0_" prefix does nothing, it's just necessary because "0b_..."
//                     ↓ 	isn't a valid form
//					   ↓ * The rest of the `_` denote the seperation between the numerator and
//                     ↓ 	denominator, strictly for human understanding only (it has no impact on
//                     ↓ 	the code's behaviour)
AnyPackedFraction(bitPattern: 0b0__00111111 as UInt8, numeratorBitWidth: 0, denominatorBitWidth: 8).test()
AnyPackedFraction(bitPattern: 0b0_0_0111111 as UInt8, numeratorBitWidth: 1, denominatorBitWidth: 7).test()
AnyPackedFraction(bitPattern: 0b0_00_111111 as UInt8, numeratorBitWidth: 2, denominatorBitWidth: 6).test()
AnyPackedFraction(bitPattern: 0b0_001_11111 as UInt8, numeratorBitWidth: 3, denominatorBitWidth: 5).test()
AnyPackedFraction(bitPattern: 0b0_0011_1111 as UInt8, numeratorBitWidth: 4, denominatorBitWidth: 4).test()
AnyPackedFraction(bitPattern: 0b0_00111_111 as UInt8, numeratorBitWidth: 5, denominatorBitWidth: 3).test()
AnyPackedFraction(bitPattern: 0b0_001111_11 as UInt8, numeratorBitWidth: 6, denominatorBitWidth: 2).test()
AnyPackedFraction(bitPattern: 0b0_0011111_1 as UInt8, numeratorBitWidth: 7, denominatorBitWidth: 1).test()
AnyPackedFraction(bitPattern: 0b0_00111111_ as UInt8, numeratorBitWidth: 8, denominatorBitWidth: 0).test()

