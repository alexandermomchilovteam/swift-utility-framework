//
//  isConsecutive.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public extension Sequence where Element: Strideable, Element.Stride: BinaryInteger {
	func isConsecutive() -> Bool {
		return zip(self, self.dropFirst()).allSatisfy { currentElement, nextElement in
			currentElement.distance(to: nextElement) == 1
		}
	}
}
