//
//  Operators.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

extension Fraction {
	public static prefix func - (_ fraction: Fraction) -> Fraction {
		return Fraction(numerator: -fraction.num, denominator: fraction.denum)
	}
	
	public static func - (lhs: Fraction, rhs: Fraction) -> Fraction {
		return lhs + (-rhs)
	}
	
	public static func + (lhs: Fraction, rhs: Fraction) -> Fraction {
		let _gcd = gcd(lhs.denum, rhs.denum)
		let lhsScale = rhs.denum / _gcd
		let rhsScale = lhs.denum / _gcd
		
		let lhsScaledNum = (lhs * lhsScale).num
		let rhsScaledNum = (rhs * rhsScale).num
		
		let lcm = (lhs.denum * rhs.denum) / _gcd
		
		return Fraction(numerator: lhsScaledNum + rhsScaledNum, denominator: lcm)
	}
	
	public static func * (lhs: Fraction, rhs: Fraction) -> Fraction {
		return Fraction(numerator: lhs.num * rhs.num, denominator: lhs.denum * rhs.denum)
	}
	
	public static func * (lhs: Fraction, rhs: Int) -> Fraction {
		return Fraction(numerator: lhs.num * rhs, denominator: lhs.denum)
	}
	
	public static func * (lhs: Int, rhs: Fraction) -> Fraction {
		return rhs * lhs
	}
}
