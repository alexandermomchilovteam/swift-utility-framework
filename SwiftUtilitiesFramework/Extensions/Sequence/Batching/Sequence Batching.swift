//
//  Sequence Batching.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-05.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension Sequence {
	public func batch(size: Int) -> BatchingSequence<Self> {
		return BatchingSequence(wrapping: self, batchSize: size)
	}
}

public struct BatchingSequence<S: Sequence>: Sequence {
	let wrappedSequence: S
	let batchSize: Int
	
	public init(wrapping sequence: S, batchSize: Int) {
		self.wrappedSequence = sequence
		self.batchSize = batchSize
	}
	
	public func makeIterator() -> BatchingIterator<S.Iterator> {
		return BatchingIterator(
			wrapping: self.wrappedSequence.makeIterator(),
			batchSize: batchSize
		)
	}
}

public struct BatchingIterator<WrappedIterator: IteratorProtocol>: IteratorProtocol {
	private var wrappedIterator: WrappedIterator
	private var progress = 0
	private let batchSize: Int
	
	public init(wrapping wrappedIterator: WrappedIterator, batchSize: Int) {
		self.wrappedIterator = wrappedIterator
		self.batchSize = batchSize
	}
	
	public mutating func next() -> [WrappedIterator.Element]? {
		var batch = [WrappedIterator.Element]()
		for _ in 0..<batchSize {
			if let next = wrappedIterator.next() {
				batch.append(next)
			}
			else {
				return batch.isEmpty ? nil : batch
			}
		}
		return batch
	}
}
