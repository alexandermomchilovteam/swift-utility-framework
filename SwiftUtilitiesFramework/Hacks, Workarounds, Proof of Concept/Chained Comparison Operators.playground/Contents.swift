
struct X: ExpressibleByIntegerLiteral {
	let value: Int
	
	init(integerLiteral value: Int) {
		self.value = value
	}
}

func <(lhs: X, rhs: X) -> (Bool, Int) {
	return (lhs.value < rhs.value, rhs.value)
}

func <(lhs: (Bool, Int), rhs: X) -> (Bool, Int) {
	return (lhs.0 && lhs.1 < rhs.value, rhs.value)
}

func <(lhs: (Bool, Int), rhs: X) -> Bool {
	return lhs.0 && lhs.1 < rhs.value
}

precedencegroup AssosiativeComparisonPrecedence {
	associativity: left
	higherThan: ComparisonPrecedence
}

infix operator <: AssosiativeComparisonPrecedence

typealias IntegerLiteralType = X
print((0 < 1 < 2 < 3 < 4) as Bool)
