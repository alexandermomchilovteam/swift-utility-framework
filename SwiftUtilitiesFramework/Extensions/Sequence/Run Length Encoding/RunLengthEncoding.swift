//
//  RunLengthEncoding.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public extension Sequence where Self.Iterator.Element: Equatable {
	func runLengthEncoded() -> LazySequenceRunLengthEncoder<Self> {
		return LazySequenceRunLengthEncoder(encoding: self)
	}
}

public struct LazySequenceRunLengthEncoder<WrappedSequence: Sequence>: Sequence
where WrappedSequence.Element: Equatable {
	public let wrappedSequence: WrappedSequence
	
	public init(encoding wrappedSequence: WrappedSequence) {
		self.wrappedSequence = wrappedSequence
	}
	
	public func makeIterator() -> RunLengthEncodingIterator<WrappedSequence.Iterator> {
		return RunLengthEncodingIterator(encoding: wrappedSequence.makeIterator())
	}
}

public struct RunLengthEncodingIterator<WrappedIterator: IteratorProtocol>: IteratorProtocol
where WrappedIterator.Element: Equatable {
	
	public private(set) var wrappedIterator: WrappedIterator
	public private(set) var currentGrouping: (element: WrappedIterator.Element, count: Int)? = nil
	
	public init(encoding wrappedIterator: WrappedIterator) {
		self.wrappedIterator = wrappedIterator
	}
	
	public mutating func next() -> (element: WrappedIterator.Element, count: Int)? {
		
		while let newElement = wrappedIterator.next() { // Take all elements of this run
			if let currentGrouping = self.currentGrouping {
				if newElement == currentGrouping.element { // increment the current run
					let newCount = currentGrouping.count + 1
					self.currentGrouping = (element: newElement, count: newCount)
				} else { // Broke the streak
					defer {
						self.currentGrouping = (element: newElement, count: 1) // start a new group
					}
					return self.currentGrouping
				}
			} else { // There is no current group, this is the first element
				self.currentGrouping = (element: newElement, count: 1)
			}
		}
		
		// Reached end of the wrapped iterator
		
		// 2. Only return the current grouping once, return the `nil` next time to end this iterator.
		defer { self.currentGrouping = nil }
		
		// 1. Return current grouping, if there is one
		return self.currentGrouping
	}
}
