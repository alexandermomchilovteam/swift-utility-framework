//
//  Social Security Number.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public struct SocialSecurityNumber {
	// Stores aaa-bb-ccc as aaabbbccc
	private let ssn: UInt32
	
	public init?(_ string: String) {
		let segments = string.split(separator: "-")
		
		guard segments.lazy.map({ $0.count }) == [3, 2, 3] else {
			debugPrint("SSN segments must be lenght 3, 2, 3 (e.g. 123-45-678).")
			return nil
		}
		
		guard !zip(segments, ["000", "00", "000"]).contains(where: {$0.0 == $0.1}) else {
			debugPrint("SSN segments cannot be all zeros.")
			return nil
		}
		
		let firstSegment = segments[0]
		guard firstSegment != "666", !firstSegment.hasPrefix("9") else {
			debugPrint("The first SSN segment (\(firstSegment)) cannot be 666, or be in the range 900-999.")
			return nil
		}
		
		let dashesRemoved = string.replacingOccurrences(of: "-", with: "")
		guard let ssn = UInt32(dashesRemoved) else { return nil }
		self.ssn = ssn
	}
}

extension SocialSecurityNumber: ExpressibleByStringLiteral {
	public init(stringLiteral literalString: String) {
		self.init(literalString)!
	}
}

extension SocialSecurityNumber: CustomStringConvertible {
	public var description: String {
		let formatter = NumberFormatter()
		
		formatter.minimumIntegerDigits = 3
		let segment1 = formatter.string(from: NSNumber(value: self.ssn / 100000))!
		
		
		formatter.minimumIntegerDigits = 2
		let segment2 = formatter.string(from: NSNumber(value: (self.ssn / 1000) % 100))!
		
		formatter.minimumIntegerDigits = 3
		let segment3 = formatter.string(from: NSNumber(value: self.ssn % 1000))!
		return "\(segment1)-\(segment2)-\(segment3)"
	}
}
