//
//  CycleSequence as UnfoldSequence.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public func makeCycleSequence<C: Collection>(for c: C) -> AnySequence<C.Iterator.Element> {
	return AnySequence(
		sequence(state: (elements: c, elementIterator: c.makeIterator()), next: { state in
			if let nextElement = state.elementIterator.next() {
				return nextElement
			}
			else {
				state.elementIterator = state.elements.makeIterator()
				return state.elementIterator.next()
			}
		})
	)
}
