//
//  FibSequence.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-12-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public protocol Summable {
    static func + (lhs: Self, rhs: Self) -> Self
}

extension Int: Summable {}
extension String: Summable {}

public struct FibSequence<Element: Summable>: Sequence {
    public let seeds: (Element, Element)
    
    public init(seeds: (Element, Element)) {
        self.seeds = seeds
    }
    
    public func makeIterator() -> FibIterator<Element> {
        return FibIterator(seeds: self.seeds)
    }
}

public extension FibSequence where Element: ExpressibleByIntegerLiteral {
    init() {
        self.init(seeds: (1, 1))
    }
}

public struct FibIterator<Element: Summable>: IteratorProtocol {
    private var a: Element
    private var b: Element
    
    public init(seeds: (Element, Element)) {
        (self.a, self.b) = seeds
    }
    
    public mutating func next() -> Element? {
        defer {
            (a, b) = (b, a + b)
        }
        return self.a
    }
}
