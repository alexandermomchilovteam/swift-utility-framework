//
//  QEncoded.swift
//  SwiftUtilities
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

// A QEncoded binary number of the form Qm.n https://en.wikipedia.org/wiki/Q_%28number_format%29
