//
//  SwiftUtilitiesFramework.h
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SwiftUtilitiesFramework.
FOUNDATION_EXPORT double SwiftUtilitiesFrameworkVersionNumber;

//! Project version string for SwiftUtilitiesFramework.
FOUNDATION_EXPORT const unsigned char SwiftUtilitiesFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftUtilitiesFramework/PublicHeader.h>


