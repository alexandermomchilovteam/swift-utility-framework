//
//  DictionaryDifference.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

extension Dictionary where Value: Equatable {
	public func diff(with other: [Key: Value]) -> (
		leftOnly: [Key: Value],
		rightOnly: [Key: Value],
		conflictingValues: [Key: (left: Value, right: Value)]
	) {
		let (left, right) = (self, other)
		var conflictingValues = [Key: (left: Value, right: Value)]()
		
		let leftOnly = left.filter { leftKey, leftValue in
			if let rightValue = right[leftKey] {
				if leftValue != rightValue {
					conflictingValues[leftKey] = (left: leftValue, right: rightValue)
				}
				return false
			}
			else {
				return true
			}
		}
		
		return (
			leftOnly: leftOnly,
			rightOnly: right.filter { k, _ in left[k] == nil },
			conflictingValues: conflictingValues
		)
	}
}
