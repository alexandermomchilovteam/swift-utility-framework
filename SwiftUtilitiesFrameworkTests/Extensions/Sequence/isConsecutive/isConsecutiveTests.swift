//
//  isConsecutiveTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest

class isConsecutiveTests: XCTestCase {
	func testEmpty() {
		XCTAssertTrue(Array<Int>().isConsecutive())
	}

	func testConsecutive() {
		XCTAssertTrue(Array<Int>(0...10).isConsecutive())
	}
	
	func testNonConsecutive() {
		XCTAssertFalse([0, 5].isConsecutive())
	}
}
