//
//  BitVectorGuts.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

// This class is responsible for storing a buffer of UInt64s, into which the bit vector is encoded,
// prefixed by a header, which stores the number of bits in the bit vector.
// This class is only responsible for memory management, etc.
// Conforming to sequence is done by BitVectorGuts
class BitVectorManagedBuffer: ManagedBuffer<BitVectorManagedBuffer.Header, UInt64> {
	struct Header {
		var bitCount: Int
	}
	
	class func newBuffer(bitCapacity: Int) -> BitVectorManagedBuffer {
		let neededUInt64Count = bitCapacity.roundUpDivide(byDivisor: 64)
		
		return BitVectorManagedBuffer.create(
			minimumCapacity: neededUInt64Count,
			makingHeaderWith: { _ in Header(bitCount: 0) }
		) as! BitVectorManagedBuffer // safe cast, per the API's contract
	}
	
	var bitCount: Int {
		get { return self.withUnsafeMutablePointerToHeader { $0.pointee.bitCount } }
		set { self.withUnsafeMutablePointerToHeader { $0.pointee.bitCount = newValue } }
	}
	
	var bitCapacity: Int { return self.capacity * 64 }

	subscript(index: BitVectorGuts.Index) -> Bool {
		get {
			return self.withUnsafeMutablePointerToElements { p in
				return p[index.quadwordIndex][bitIndex: index.bitIndex]
			}
		}
		set {
			self.withUnsafeMutablePointerToElements { p in
				p[index.quadwordIndex][bitIndex: index.bitIndex] = newValue
			}
		}
	}
}

extension Int {
	func roundUpDivide(byDivisor divisor: Int) -> Int {
		return (self + divisor - 1) / divisor
	}
}

/// This struct is a sequence-conforming wrapper for a BitVectorManagedBuffer
struct BitVectorGuts {
	var buffer: BitVectorManagedBuffer?

	// The number of bits
	// (which is different from the number of UInt64 storing those bits
	var count: Int {
		get { return buffer?.bitCount ?? 0 }
		set { buffer?.bitCount = newValue }
	}
	
	var capacity: Int { return self.buffer?.bitCapacity ?? 0 }
}



extension BitVectorGuts: Sequence {
	typealias Index = BitVector.Index
// Specialize if necessary
//	struct Iterator: IteratorProtocol {
//		typealias Element = Bool
//
//		let bitVectorGuts: BitVectorGuts
//		let index: BitVectorGuts._Index
//
//		init(_ bitVectorGuts: BitVectorGuts) {
//			self.bitVectorGuts = bitVectorGuts
//			self.index = bitVectorGuts.startIndex
//		}
//
//		mutating func next() -> Bool? {
//			return false
//		}
//	}
//	func makeIterator() -> BitVectorGuts.Iterator {
//		return BitVectorGuts.Iterator(self)
//	}
}

extension BitVectorGuts: Collection {

	var startIndex: Index { return Index(0) }
	var endIndex: Index { return Index(count) }

	subscript(_ int: Int) -> Bool {
		get { return self[Index(int)] }
		set { self[Index(int)] = newValue }
	}
	
	private func assertValidIndex(_ index: Index) {
		assert(self.indices.contains(index), "Index out of range")
	}
	
	subscript(index: Index) -> Bool {
		get {
			assertValidIndex(index)
			return self.buffer![index]
		}
		set {
			assertValidIndex(index)
			self.buffer![index] = newValue
		}
	}

	func index(after i: Index) -> Index {
		return i.successor
	}
}

extension BitVectorGuts: MutableCollection {
	/* Subscript setter already defined above */
}

extension BitVectorGuts: BidirectionalCollection {
	func index(before i: Index) -> Index {
		return i.predeccessor
	}
}

extension BitVectorGuts: RandomAccessCollection {}



//extension BitVectorGuts: RangeReplaceableCollection {
//
//}

extension BitVectorGuts {
	init(initialCapacity: Int = 0) {
		if initialCapacity == 0 {
			self.buffer = nil
		} else {
			self.buffer = BitVectorManagedBuffer.newBuffer(bitCapacity: initialCapacity)
		}
	}
	
	init<S: Sequence>(_ source: S) where S.Element == Bool {
		let initialCapacity = source.underestimatedCount
		self.init(initialCapacity: initialCapacity)
		
		var iterator = source.makeIterator()
		
		// Add elements up to the initial capacity without checking for regrowth.
		for _ in 0..<initialCapacity {
			self.append(iterator.next()!)
		}
		
		// Add remaining elements, if any.
		while let element = iterator.next() {
			self.append(element)
		}
	}
	
	// https://github.com/apple/swift/blob/74d1322a727ac903bf518fa79fdab5221bb3274c/stdlib/public/core/ArrayShared.swift#L130-L133
	@inlinable
	internal func _growArrayCapacity(_ capacity: Int) -> Int {
		return capacity * 2
	}
	
	public mutating func reserveCapacity(_ minimumCapacity: Int) {
		if minimumCapacity <= self.capacity { return }
		
//		print("reserving capacity: \(minimumCapacity)")
		
		guard let oldBuffer = buffer else {
//			print("Allocating new buffer with bitCount: \(minimumCapacity)")
			self.buffer = BitVectorManagedBuffer.newBuffer(bitCapacity: minimumCapacity)
			return
		}
		
		
		
		let newCapacity = _growArrayCapacity(minimumCapacity)
		let newBuffer = BitVectorManagedBuffer.newBuffer(bitCapacity: newCapacity)
		
//		print("Growing to size: \(newCapacity)")
		oldBuffer.withUnsafeMutablePointerToElements { oldElementPtr in
			newBuffer.withUnsafeMutablePointerToElements { newElementPtr in
				newElementPtr.moveInitialize(from: oldElementPtr, count: oldBuffer.capacity)
			}
		}
		newBuffer.bitCount = oldBuffer.bitCount
		
		self.buffer = newBuffer
	}
	
	var conciseDescription: String {
		if self.isEmpty { return "(empty)" }
		return self.map { $0 ? "1" : "0" }.joined(separator: "")
	}
	
	public mutating func append(_ newElement: Bool) {
		reserveCapacity(self.count + 1)

		self.count += 1
	}
}

