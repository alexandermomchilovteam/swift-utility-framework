func ~=<T>(pattern: (T) -> Bool, value: T) -> Bool {
	return pattern(value)
}

typealias BoolInstanceMethod<T, U> = (_ instance: T) -> (_ arg: U) -> Bool

func apply<T, U>(instanceMethod: @escaping BoolInstanceMethod<T, U>) ->
	(_ arg: U) -> (_ instance: T) -> Bool {
		return { arg in
			return { instance in
				return instanceMethod(instance)(arg)
			}
		}
}

let s = "The quick brown fox"
let hasPrefix = apply(instanceMethod: String.hasPrefix)

switch s {
case hasPrefix("The"): print("The")
case hasPrefix("Foo"): print("Foo")
default: print("default")
}
