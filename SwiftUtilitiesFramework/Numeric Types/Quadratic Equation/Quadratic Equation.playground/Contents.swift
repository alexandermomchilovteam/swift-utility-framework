import SwiftUtilitiesFramework

let a = 5 ± 10
print(a)
print(a + 2)
print(a - 2)
print(a * 2)
//print(a / 2)

print(2 + a)
print(2 - a)
print(2 * a)
//print(a / 2)

print(QuadraticRoots.solveFrom(xSquared: 1, x: 4, c: 3))
