//
//  UniquedTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-06-11.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import SwiftUtilitiesFramework

fileprivate struct EquatableIntWrapper: Equatable, ExpressibleByIntegerLiteral {
	let i: Int
	
	init(integerLiteral: Int) {
		self.i = integerLiteral
	}
}

class UniquedTests: XCTestCase {
	func testEquatableRemovingDuplicates() {
		let input = [1, 1, 1, 2, 2, 3] as [EquatableIntWrapper]
		let actual = input.removingDuplicates()
		XCTAssertEqual(actual, [1, 2, 3])
    }

	func testEquatableRemoveDuplicates() {
		var input = [1, 1, 1, 2, 2, 3] as [EquatableIntWrapper]
		input.removeDuplicates()
		XCTAssertEqual(input, [1, 2, 3])
    }
	
	func testHashableRemovingDuplicates() {
		let input = [1, 1, 1, 2, 2, 3]
		let actual = input.removingDuplicates()
		XCTAssertEqual(actual, [1, 2, 3])
	}
	
	func testHashableRemoveDuplicates() {
		var input = [1, 1, 1, 2, 2, 3]
		input.removeDuplicates()
		XCTAssertEqual(input, [1, 2, 3])
	}
}
