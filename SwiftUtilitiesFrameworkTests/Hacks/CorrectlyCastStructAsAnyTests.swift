//
//  CorrectlyCastStructAsAny.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

// https://bugs.swift.org/browse/SR-3871

import XCTest
import Foundation

fileprivate protocol P {}
fileprivate struct S: P {}

fileprivate class Wrapper {
	@objc dynamic let any: Any = S() //dynamic ensures the struct will be boxed into _SwiftValue
	init() {}
}


class CorrectlyCastStructAsAny: XCTestCase {

    func testBugStilExists() {
		XCTAssertFalse(Wrapper().any is P, "https://bugs.swift.org/browse/SR-3871 has been fixed!")
    }
	
	func testWorkaroundTypeCheckWorks() {
		XCTAssertTrue(Wrapper().any as AnyObject is P)
		XCTAssertTrue(typecheck(isAny: Wrapper().any, ofType: P.self))
	}
	
	func testWorkaroundTypeCastWorks() {
		XCTAssertNotNil(Wrapper().any as AnyObject as? P)
		XCTAssertNotNil(cast(any: Wrapper().any, to: P.self))
	}
}
