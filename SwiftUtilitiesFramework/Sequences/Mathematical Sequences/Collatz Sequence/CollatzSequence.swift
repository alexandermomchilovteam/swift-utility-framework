//
//  CollatzSequence.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public struct CollatzSequence: Sequence {
	public let n: Int
	
	public init(startingAt n: Int) { self.n = n }
	
	public func makeIterator() -> CollatzIterator {
		return CollatzIterator(startingAt: n)
	}
}

public struct CollatzIterator: IteratorProtocol {
	private var n: Int
	private var done = false
	
	public init(startingAt n: Int) { self.n = n }
	
	public mutating func next() -> Int? {
		if n == 1 {
			if done { return nil }
			defer { done = true }
			
			return 1
		}
		
		defer {
			n = n.isMultiple(of: 2)
				? n/2 // even case
				: 3*n + 1 // odd case
		}
		
		return n
	}
}
