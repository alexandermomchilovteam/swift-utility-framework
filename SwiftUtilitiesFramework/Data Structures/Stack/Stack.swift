//
//  Stack.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-10.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

struct Stack<T> {
	var elements: [T]

	init() { self.elements = [] }

	func peek() -> T? { return self.elements.last }
	mutating func pop() -> T? { return self.elements.popLast() }
	mutating func push(_ newElement: T) { return self.elements.append(newElement) }
}
