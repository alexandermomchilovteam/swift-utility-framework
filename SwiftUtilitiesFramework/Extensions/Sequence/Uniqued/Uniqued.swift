//
//  Sequence - Unique.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public extension Sequence where Element: Hashable {
	// O(self.unique.count) in space
	// O(self.count) in time
	func removingDuplicates() -> [Iterator.Element] {
		var alreadySeen = Set<Iterator.Element>()
		return self.filter { alreadySeen.insert($0).inserted }
	}
}

public extension RangeReplaceableCollection where Element: Hashable {
	// O(self.unique.count) in space
	// O(self.count) in time
	mutating func removeDuplicates() {
		var alreadySeen = Set<Iterator.Element>()
		self.removeAll(where: { alreadySeen.insert($0).inserted })
	}
}


public extension Sequence where Element: Equatable {
	// O(self.unique.count) in space
	// O(self.count^2) in time
	func removingDuplicates() -> [Element] {
		var alreadySeen = [Element]()
		return self.filter {
			if alreadySeen.contains($0) { return false }
			alreadySeen.append($0)
			return true
		}
	}
}

public extension RangeReplaceableCollection where Element: Equatable {
	// O(self.unique.count) in space
	// O(self.count^2) in time
	mutating func removeDuplicates() {
		var alreadySeen = [Element]()
		self.removeAll(where: { element -> Bool in
			if alreadySeen.contains(element) { return true }
			alreadySeen.append(element)
			
			return false
		})
	}
}
