import SwiftUtilitiesFramework

let s1 = CycleSequence(cycling: [1, 2, 3]) // Works with arrays of numbers, as you would expect.
// Taking one element at a time, manually
var i1 = s1.makeIterator()
print(i1.next() as Any)
print(i1.next() as Any)
print(i1.next() as Any)
print(i1.next() as Any)
print(i1.next() as Any)
print(i1.next() as Any)
print(i1.next() as Any)

let s2 = CycleSequence(cycling: 2...5) // Works with any Collection. Ranges work!
// Taking the first 10 elements
print(Array(s2.prefix(10)))

let s3 = CycleSequence(cycling: "abc") // Strings are Collections, so those work, too!
s3.prefix(10).map{ "you can even map over me! \($0)" }.forEach{ print($0) }


print(Array(CycleSequence(cycling: [true, false]).prefix(7)))
print(Array(CycleSequence(cycling: 1...3).prefix(7)))
print(Array(CycleSequence(cycling: "ABC").prefix(7)))
print(Array(CycleSequence(cycling: EmptyCollection<Int>()).prefix(7)))
print(Array(zip(1...10, CycleSequence(cycling: "ABC"))))

let repeater = makeCycleSequence(for: [1, 2, 3])
print(Array(repeater.prefix(10)))

