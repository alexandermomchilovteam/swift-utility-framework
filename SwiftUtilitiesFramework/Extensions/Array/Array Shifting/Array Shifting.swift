// https://stackoverflow.com/a/39153003/3141234

extension Array {
	public func shiftedLeft(by rawOffset: Int = 1) -> Array {
		let clampedAmount = rawOffset % count
		let offset = clampedAmount < 0 ? count + clampedAmount : clampedAmount
		return Array(self[offset ..< count] + self[0 ..< offset])
	}
	
	public func shiftedRight(by rawOffset: Int = 1) -> Array {
		return self.shiftedLeft(by: -rawOffset)
	}
	
	public mutating func shiftLeftInPlace(by rawOffset: Int = 1) {
		if rawOffset == 0 { return /* no-op */ }
		
        func shiftedIndex(for index: Int) -> Int {
			let candidateIndex = (index + rawOffset) % self.count
			
			if candidateIndex < 0 {
				return candidateIndex + self.count
			}
			
			return candidateIndex
		}
		
		// Create a sequence of indexs of items that need to be swapped.
		//
		// For example, to shift ["A", "B", "C", "D", "E"] left by 1:
		// Swapping 2 with 0: ["C", "B", "A", "D", "E"]
		// Swapping 4 with 2: ["C", "B", "E", "D", "A"]
		// Swapping 1 with 4: ["C", "A", "E", "D", "B"]
		// Swapping 3 with 1: ["C", "D", "E", "A", "B"] <- Final Result
		//
		// The sequence here is [0, 2, 4, 1, 3].
		// It's turned into [(2, 0), (4, 2), (1, 4), (3, 1)] by the zip/dropFirst trick below.
        let closure: (Int) -> Int? = { index in
            let nextIndex = shiftedIndex(for: index)
            if nextIndex == 0 { return nil } // We've come full-circle
            return nextIndex
        }
        
        withoutActuallyEscaping(closure) { closure in // # TODO: improve this.
            let indexes = sequence(first: 0, next: closure)
        
            print(self)
            for (source, dest) in zip(indexes.dropFirst(), indexes) {
                self.swapAt(source, dest)
                print("Swapping \(source) with \(dest): \(self)")
            }
            print(Array<(Int, Int)>(zip(indexes.dropFirst(), indexes)))
        }
	}
	
	public mutating func shiftRightInPlace(by rawOffset: Int = 1) {
		self.shiftLeftInPlace(by: rawOffset)
	}
}

public func << <T>(array: [T], offset: Int) -> [T] { return array.shiftedLeft(by: offset) }
public func >> <T>(array: [T], offset: Int) -> [T] { return array.shiftedRight(by: offset) }
public func <<= <T>(array: inout [T], offset: Int) { return array.shiftLeftInPlace(by: offset) }
public func >>= <T>(array: inout [T], offset: Int) { return array.shiftRightInPlace(by: offset) }
