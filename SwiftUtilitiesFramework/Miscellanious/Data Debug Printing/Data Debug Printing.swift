//
//  Data Debug Printing.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-05.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension String {
	init<T: FixedWidthInteger>(_ source: T, radix: Int, uppercase: Bool = false, width minWidth: Int) {
		var result: [String] = []
		for i in 0..<(T.bitWidth / 8) {
			let byte = UInt8(truncatingIfNeeded: source >> (i * 8))
			let byteString = String(byte, radix: radix, uppercase: uppercase)
			result.append(byteString)
		}
		
		let unpadded = result.reversed().joined()
		self = unpadded.leftPadded(with: "0", minWidth: minWidth)
	}
	
	func leftPadded(with paddingCharacter: Character, minWidth: Int) -> String {
		if paddingCharacter == "_" {
			print("\"\(self)\".padLeft(with: \"\(paddingCharacter)\", minWidth: \(minWidth)")
		}
		let padding = String(repeating: paddingCharacter, count: Swift.max(0, minWidth - self.count))
		return padding + self
	}
}


extension Data {
	var debugBinaryString: String {
		return DataDebugPrinter().debugString(for: self)
	}
}

struct DataDebugPrinter {
	enum SegmentType {
		case decimalOffset, binary, hex
		
		var width: Int {
			switch self {
			case .decimalOffset: return 6
			case .binary: return 71 // 64 bits + 7 spaces
			case .hex: return 19 // 16 hexits + 3 spaces
			}
		}
		
		var headerLines: (line1: String, line2: String) {
			switch self {
			case .decimalOffset: return (line1: "      ", line2: "offset")
			case .binary: return (
					line1: "66666555 55555554 44444444 43333333 33322222 22222111 11111110 00000000",
					line2: "43210987 65432109 87654321 09876543 21098765 43210987 65432109 87654321"
				)
			case .hex: return (line1: "1111 1100 0000 0000", line2: "6543 2109 8765 4321")
			}
			
		}
		
		func segment(for uint64: UInt64, offset: Int) -> String {
			switch self {
			case .decimalOffset:
				return String(offset).leftPadded(with: " ", minWidth: self.width)
				
			case .binary:
				let bytes = (0..<8).map { i in UInt8(truncatingIfNeeded: uint64 >> (i * 8)) }
				let byteStrings = bytes.map { String($0, radix: 2, width: 8) }.joined(separator: " ")
				return byteStrings
				
			case .hex:
				let words = (0..<4).map { i in UInt16(truncatingIfNeeded: uint64 >> (i * 16)) }
				let wordStrings = words.map { String($0, radix: 16, width: 4) }.joined(separator: " ")
				return wordStrings
			}
		}
	}
	
	let segments: [SegmentType] = [.decimalOffset, .binary, .hex]
	
	private func headerLine1() -> String {
		return self.segments.map { $0.headerLines.line1 }.joined(separator: " ┃ ")
	}
	
	private func headerLine2() -> String {
		return self.segments.map { $0.headerLines.line2 }.joined(separator: " ┃ ")
	}
	
	private func crossLine() -> String {
		return self.segments.map { String(repeating: "━", count: $0.width) }.joined(separator: "━╋━")
	}
	
	private func row(for uint64: UInt64, offset: Int) -> String {
		return self.segments.map { $0.segment(for: uint64, offset: offset) }.joined(separator: " ┃ ")
	}
	
	public func debugString(for data: Data) -> String {
		let rows = data.withUnsafeBytes { rawBufferPointer -> [String] in
			// TODO: fixing rounding issue
			let uint64s = rawBufferPointer.bindMemory(to: UInt64.self)
		
			return uint64s.enumerated().map { uint64Offset, uint64 -> String in
				let byteOffset = uint64Offset * 8
				
				return self.row(for: uint64, offset: byteOffset)
			}
		}
		return """
		\(headerLine1())
		\(headerLine2())
		\(crossLine())
		\(rows.joined(separator: "\n"))
		"""
	}
}
