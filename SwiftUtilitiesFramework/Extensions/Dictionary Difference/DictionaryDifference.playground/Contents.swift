import SwiftUtilitiesFramework

let a = [
	"a": 1,
	"b": 2,
	"c": 3,
	"d": 4,
]

let b = [
	"b": 2,
	"c": 9,
	"d": 4,
	"e": 5,
]

print(a.diff(with: b))
