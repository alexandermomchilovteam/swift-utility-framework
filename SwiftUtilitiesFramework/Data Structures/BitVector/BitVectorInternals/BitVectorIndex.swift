//
//  BitVectorIndex.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension BitVector {
	struct Index: Equatable, Hashable {
		static let quadwordSize = 63
		static let bitIndexMask: Int = 0b11_1111
		static let quadwordIndexMask = ~Index.bitIndexMask

		let i: Int
		
		init(_ i: Int) { self.i = i }

		private func assertInvarients() {
			assert(0 <= i, "The index can only be a positive signed number.")
		}

		var bitIndex: Int {
			assertInvarients()
			return i & Index.bitIndexMask
		}

		var quadwordIndex: Int {
			assertInvarients()
			return i & Index.quadwordIndexMask
		}

		var successor: Index { return Index(self.i &+ 1) } // TODO: handle overflow
		var predeccessor: Index { return Index(self.i &- 1) } // TODO: handle overflow

	}
}

extension BitVector.Index: Comparable {
	static func < (lhs: BitVector.Index, rhs: BitVector.Index) -> Bool {
		return lhs.i < rhs.i
	}
}

extension BitVector.Index: Strideable {
	typealias Stride = Int

	func distance(to other: BitVector.Index) -> Int {
		return other.i - self.i
	}

	func advanced(by n: Int) -> BitVector.Index {
		return BitVector.Index(self.i + n)
	}
}
