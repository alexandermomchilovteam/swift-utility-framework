// https://stackoverflow.com/a/42034523/3141234
// https://bugs.swift.org/browse/SR-3871

// This is an instance of the general "runtime dynamic casting doesn't
// bridge if necessary to bridge to a protocol" bug. Since sender is seen
// as _SwiftValue object type, and we're trying to get to a protocol it
// doesn't conform to, we give up without also trying the bridged type.

// Weirdly enough, first casting to AnyObject and then casting to the protocol appears to work:

func cast<T>(any: Any, to: T.Type) -> T? {
	return (any as? T) ?? (any as AnyObject as? T)
}

func typecheck<T>(isAny any: Any, ofType: T.Type) -> Bool {
	return (any is T) || (any as AnyObject is T)
}
