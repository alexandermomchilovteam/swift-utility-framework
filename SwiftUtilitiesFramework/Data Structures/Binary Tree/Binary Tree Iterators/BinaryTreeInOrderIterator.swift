//
//  BinaryTreeInOrderIterator.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-30.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public extension BinaryTreeNodeProtocol {
	var inorderTraversal: BinaryTreeInorderSequence<Self> { return BinaryTreeInorderSequence(self) }
}

public struct BinaryTreeInorderSequence<Tree: BinaryTreeNodeProtocol>: Sequence {
	private let tree: Tree
	
	public init(_ tree: Tree) { self.tree = tree }
	
	public func makeIterator() -> BinaryTreeInorderIterator<Tree> {
		return BinaryTreeInorderIterator(tree: self.tree)
	}
}

public struct BinaryTreeInorderIterator<Tree: BinaryTreeNodeProtocol>: IteratorProtocol {
	private var node: Tree?
	private var backtrackStack = [Tree]()
	
	public init(tree: Tree) {
		self.node = tree
	}
	
	public mutating func next() -> Tree.Payload? {
		while let leftChild = self.node {
			backtrackStack.append(leftChild)
			self.node = leftChild.left
		}
		
		guard !backtrackStack.isEmpty else { return nil }
		
		let current = backtrackStack.removeLast()
		self.node = current.right
		return current.payload
	}
}
