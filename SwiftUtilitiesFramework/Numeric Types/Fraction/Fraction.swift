//
//  Fraction.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public struct Fraction {
	public let num: Int
	public let denum: Int
	
	public init(numerator: Int, denominator: Int) {
		precondition(denominator != 0, "Fractions cannot have a 0 denominator!")
		self.num = numerator
		self.denum = denominator
	}
}

extension Fraction: CustomDebugStringConvertible {
	public var debugDescription: String { return "\(num)/\(denum) (\(Double(self)))"}
}
