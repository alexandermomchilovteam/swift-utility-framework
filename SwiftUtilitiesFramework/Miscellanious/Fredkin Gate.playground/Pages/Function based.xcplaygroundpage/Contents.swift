//: [Previous](@previous)

func fredkinGate<T>(a: T, b: T, c: Bool) -> (a: T, b: T, c: Bool) {
	return c ? (b, a, c) : (a, b, c)
}

func fredkinNot(_ a: Bool) -> Bool {
	return fredkinGate(a: true, b: false, c: a).a
}

func fredkinAND(_ a: Bool, _ b: Bool) -> Bool {
	return fredkinGate(a: false, b: a, c: b).a
}

func fredkinOR(_ a: Bool, _ b: Bool) -> Bool {
	return fredkinGate(a: a, b: true, c: b).a
}

func fredkinXOR(_ a: Bool, _ b: Bool) -> Bool {
	let (_a, _b, _) = fredkinGate(a: false, b: true, c: a)
	return fredkinGate(a: _a, b: _b, c: b).a
}

func fredkinNAND(_ a: Bool, _ b: Bool) -> Bool {
	let (_a, _b, _) = fredkinGate(a: false, b: a, c: a)
	return fredkinGate(a: _b, b: _a, c: b).a
}

func fredkinNOR(_ a: Bool, _ b: Bool) -> Bool {
	let (_a, _, _) = fredkinGate(a: true, b: false, c: a)
	return fredkinGate(a: _a, b: false, c: b).a
}

func fredkinXNOR(_ a: Bool, _ b: Bool) -> Bool {
	let (_a, _b, _) = fredkinGate(a: true, b: false, c: a)
	return fredkinGate(a: _a, b: _b, c: b).a
}


let (t, f) = (true, false)

func printTruthTable(_ title: String, _ fn: (Bool, Bool) -> Bool) {
	print(title)
	print(fn(f, f))
	print(fn(f, t))
	print(fn(t, f))
	print(fn(t, t))
}

print("NOT:")
print(fredkinNot(t))
print(fredkinNot(f))

printTruthTable("\nAND:", fredkinAND)
printTruthTable("\nOR:", fredkinOR)
printTruthTable("\nXOR:", fredkinXOR)

printTruthTable("\n\n\nNAND:", fredkinNAND)
printTruthTable("\nNOR:", fredkinNOR)
printTruthTable("\nXNOR:", fredkinXNOR)


//: [Next](@next)
