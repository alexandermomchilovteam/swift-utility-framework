//
//  Bridging - Double.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

extension Fraction {
	// Work in progress
	init(_ d: Double) {
		let inverse = 1 / d
		let wholePart = inverse.rounded(.towardZero)
		let remainder = inverse - wholePart
		if 0.00001 < abs(remainder) {
			self.init(numerator: 1, denominator: Int(Double(Fraction(Int(wholePart)) + Fraction(remainder))))
		}
		else {
			self.init(numerator: 1, denominator: Int(Double(Fraction(Int(wholePart)))))
		}
	}
}

extension Double {
	init(_ fraction: Fraction) {
		self = Double(fraction.num) / Double(fraction.denum)
	}
	
	init(_ reducedFraction: ReducedFraction) {
		self.init(reducedFraction.frac)
	}
}
