//
//  Packed Fraction 16 bits.swift
//  SwiftUtilities
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public struct PackedFraction16_0: PackedFraction {
	public static let numeratorBitWidth = 16
	
	public var i: UInt16
	public init(bitPattern: I) { self.i = bitPattern }
}

public struct PackedFraction8_8: PackedFraction {
	public static let numeratorBitWidth = 8
	
	public var i: UInt16
	public init(bitPattern: I) { self.i = bitPattern }
}

public struct PackedFraction4_12: PackedFraction {
	public static let numeratorBitWidth = 4
	
	public var i: UInt16
	public init(bitPattern: I) { self.i = bitPattern }
}
