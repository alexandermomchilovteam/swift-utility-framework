//
//  BinaryTreePostorderIterator.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-31.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public extension BinaryTreeNodeProtocol {
	var postorderTraversal: BinaryTreePostorderSequence<Self> { return BinaryTreePostorderSequence(self) }
}

public struct BinaryTreePostorderSequence<Tree: BinaryTreeNodeProtocol>: Sequence {
	private let tree: Tree
	
	public init(_ tree: Tree) { self.tree = tree }
	
	public func makeIterator() -> BinaryPostorderIterator<Tree> {
		return BinaryPostorderIterator(tree: self.tree)
	}
}


//func === <T: AnyObject>(lhs: T?, rhs: T?) -> Bool {
//	switch (lhs, rhs) {
//	case ( _?, nil): return false
//	case (nil,  _?): return false
//	case (nil, nil): return true
//	case let ( a?,  b?): return a === b
//	}
//}

public struct BinaryPostorderIterator<Tree: BinaryTreeNodeProtocol>: IteratorProtocol {
	private var backtrackStack: [Tree]
	
	public init(tree: Tree) {
		self.backtrackStack = []
		findNextLeaf(node: tree)
	}
	
	private mutating func findNextLeaf(node: Tree?) {
		var node = node
    	while let _node = node {
       		backtrackStack.append(_node)
	
			if let leftChild = _node.left {
				node = leftChild
			}
			else {
				node = _node.right
			}
		}
   }
	
	public mutating func next() -> Tree.Payload? { // FIXME
		print("""
		next()
			start stack: \(backtrackStack)
		""")
		let res = backtrackStack.popLast()
		
		if let top = backtrackStack.last {
			if res === top.left {
				findNextLeaf(node: top.right) // find next leaf in right sub-tree
			}
		}
		
		print("""
			finishing stack: \(backtrackStack)
			result: \(res?.payload as Any)
		""")
		return res?.payload
	}
}


