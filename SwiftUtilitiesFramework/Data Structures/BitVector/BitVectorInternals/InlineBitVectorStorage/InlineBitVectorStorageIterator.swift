//
//  InlineBitVectorStorageIterator.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-28.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

// TODO: compare performance to indexing iterator
extension InlineBitVectorStorage {
	struct Iterator {
		var bitVector: InlineBitVectorStorage
		var count: Int
		var index: Int
		
		init(iteartingOver bitVector: InlineBitVectorStorage) {
			self.bitVector = bitVector
			self.count = bitVector.count
			self.index = 0
		}
	}
}

extension InlineBitVectorStorage.Iterator: IteratorProtocol {
	typealias Element = Bool
	
	mutating func next() -> Bool? {
		if self.index == self.count { return nil }
		self.index += 1
		
		return self.bitVector[self.index]
	}
}
