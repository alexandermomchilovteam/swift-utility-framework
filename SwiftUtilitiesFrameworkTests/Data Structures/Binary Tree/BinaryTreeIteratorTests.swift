//
//  BinaryTreeIteratorTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-05-30.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import SwiftUtilitiesFramework

class BinaryTreeIteratorTests: XCTestCase {
	func sampleTree() -> BinaryTreeNode<Int> {
		typealias B = BinaryTreeNode
		
		//      4
		//    /   \
		//   2     6
		//  / \   / \
		// 1   3 5   7
		return B(4,
			left: B(2,
				left: B(1),
				right: B(3)
			),
			right: B(6,
				left: B(5),
				right: B(7)
			)
		)
	}
	
	func wikipediaSampleTree() -> BinaryTreeNode<Character> {
		// https://w.wiki/4eV
		//      F
		//    /  \
		//   B    G
		//  / \    \
		// A   D    I
		//    / \   /
		//   C   E  H

		typealias B = BinaryTreeNode

		return B("F",
			left: B("B",
				left: B("A"),
				right: B("D",
					left: B("C"),
					right: B("E")
				)
			),
			right: B("G",
				left: nil,
				right: B("I",
					left: B("H"),
					right: nil
				)
			)
		)
	}
	
    func testPreorderTraversal() {
		XCTAssertEqual(
			Array(sampleTree().preorderTraversal),
			[4, 2, 1, 3, 6, 5, 7])
		XCTAssertEqual(String(wikipediaSampleTree().preorderTraversal), "FBADCEGIH")
    }
	
	func testInorderTraversal() {
		XCTAssertEqual(Array(sampleTree().inorderTraversal), [1, 2, 3, 4, 5, 6, 7])
		XCTAssertEqual(String(wikipediaSampleTree().inorderTraversal), "ABCDEFGHI")
	}
	
	func testPostorderTraversal() {
		XCTAssertEqual(Array(sampleTree().postorderTraversal), [1, 3, 2, 5, 7, 6, 4])
		XCTAssertEqual(String(wikipediaSampleTree().postorderTraversal), "ACEDBHIGF")
	}
}
