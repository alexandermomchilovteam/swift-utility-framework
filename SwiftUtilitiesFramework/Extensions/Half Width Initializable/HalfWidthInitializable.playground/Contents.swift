import SwiftUtilitiesFramework

assert(UInt16(high: 0x12		as UInt8 ,	low: 0x34			as UInt8 ) == 0x1234				)
assert(UInt32(high: 0x1234		as UInt16,	low: 0x5678			as UInt16) == 0x1234_5678			)
assert(UInt64(high: 0x1234_5678 as UInt32,	low: 0x90AB_CDEF 	as UInt32) == 0x1234_5678_90AB_CDEF	)
assert( Int16(high: 0x12		as UInt8 ,	low: 0x34			as UInt8 ) == 0x1234				)
assert( Int32(high: 0x1234		as UInt16,	low: 0x5678			as UInt16) == 0x1234_5678			)
assert( Int64(high: 0x1234_5678 as UInt32,	low: 0x90AB_CDEF	as UInt32) == 0x1234_5678_90AB_CDEF	)

