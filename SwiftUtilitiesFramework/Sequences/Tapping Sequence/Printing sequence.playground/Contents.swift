import SwiftUtilitiesFramework

public func printingSequence<S>(_ sequence: S) -> TappingSequence<S> {
	print("Making a sequence wrapping \(sequence) of type \(S.self)")
	return TappingSequence(wrapping: sequence) {
		let asString = $0.map { String(describing: $0) } ?? "nil"
		print("Yielding \(asString)")
	}
}

let joinedSequence = [
	printingSequence([1, 2, 3].lazy),
	printingSequence([4, 5, 6].lazy)
	].joined()

var joinedIterator = joinedSequence.makeIterator()

print("\nAbout to start the loop")
while let i = joinedIterator.next() {
	print("\tloop body \(i)")
}
