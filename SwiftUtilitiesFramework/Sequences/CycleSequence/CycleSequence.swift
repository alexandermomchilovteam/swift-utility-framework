//
//  Collection Repeater.swift
//  SwiftUtilities
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.

// Adding this for searchability, this is a previous term I used.
// https://stackoverflow.com/a/46210298/3141234
typealias SequenceRepeater = CycleSequence

// https://stackoverflow.com/a/54776466/3141234
public struct CycleSequence<C: Collection>: Sequence {
	public let cycledElements: C
	
	public init(cycling cycledElements: C) {
		self.cycledElements = cycledElements
	}
	
	public func makeIterator() -> CycleIterator<C> {
		return CycleIterator(cycling: cycledElements)
	}
}

public struct CycleIterator<C: Collection>: IteratorProtocol {
	public let cycledElements: C
	public private(set) var cycledElementIterator: C.Iterator
	
	public init(cycling cycledElements: C) {
		self.cycledElements = cycledElements
		self.cycledElementIterator = cycledElements.makeIterator()
	}
	
	public mutating func next() -> C.Iterator.Element? {
		if let next = cycledElementIterator.next() {
			return next
		} else {
			self.cycledElementIterator = cycledElements.makeIterator() // Cycle back again
			return cycledElementIterator.next()
		}
	}
}

