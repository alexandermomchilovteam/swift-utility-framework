//
//  CollatzSequenceLengthCalculator.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.

//  https://stackoverflow.com/a/44147738/3141234

struct CollatzSequenceLengthCalculator {
	// Mapping starting integers to the length of their Collatz sequence
	var memoizedCounts = [1: 1]

	// * Shared instance to save cost of reallocation.
	// * Keeps track of the trajectory followed so far, so that when a momized case is hit,
	//   all members of the trajectory can be back filled.
	//    * For example, for the trajectory starting at 5 ([5, 16, 8, 4, 2, 1]), assuming the
	//      memoizedCounts is [1: 1], the sequence is evaluated all the way up to 1.
	//      If someone then computed the trajectory starting at 10 ([10, 5, 16, 8, 4, 2, 1]),
	//      the sequence will be evaluateed all the way up to 1, repeating the work for
	//      [5, 16, 8, 4, 2]. In this case, the previousElements array would have held
	//      [5, 16, 8, 4, 2], and the Collatz sequence lengths would be backfille so that the
	//      memoized counts would be [5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1]. When this
	//      approach is used, starting at 10 only requires evaluating up til 5.
	var previousElements = [Int]()

	mutating func lengthOfSequence(startingAt n: Int) -> Int {
//		print("====== n: \(n)")
		if let existingCount = memoizedCounts[n] {
//			print("Cache hit: counts[\(n)] is \(existingCount)")
			return existingCount
		}

		previousElements.removeAll(keepingCapacity: true)

		for i in CollatzSequence(startingAt: n) {
			guard let existingCount = memoizedCounts[i] else {
//				print("No existing count for \(i)")
				previousElements.append(i)
				continue
			}

//			print("Cache hit: counts[\(i)] is \(existingCount).")
//			print("Going back to fill in previous these cache misses: \(previousElements)")

			for (offset, element) in previousElements.reversed().enumerated() {
				let newCount = offset + existingCount + 1
//				print("\tSetting counts[\(element)] to \(newCount)")
				memoizedCounts[element] = newCount
			}

			return existingCount + previousElements.count
		}

		fatalError("This should never happen")
	}
}
