//
//  main.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

//print("Hello world!")
//let emptyBitVector = BitVectorGuts(bitCount: 0)
//print(Array(emptyBitVector))
//
//let bitVector1 = BitVectorGuts([true, false, true, true, false, true])
//print(Array(bitVector1))

var v = InlineBitVectorStorage()

func p(_ v: InlineBitVectorStorage) {
	print(UnsafeRawPointer(bitPattern: UInt(v.bits)) as Any)
}

p(v)

for _ in 0...100 {
	v.count += 1
	p(v)
}
