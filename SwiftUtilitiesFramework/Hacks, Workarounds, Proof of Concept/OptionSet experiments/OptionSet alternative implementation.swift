//
//  OptionSet alternative implementation.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

protocol OptionSet2: SetAlgebra, RawRepresentable where
	Element: RawRepresentable,
	Element.RawValue: BinaryInteger,
Self.RawValue == Element.RawValue {
	
	var rawValue: RawValue { get }
	init()
	init(rawValue: RawValue)
}

extension OptionSet2 {
	func               union(_ other: Self) -> Self { return Self.init(rawValue: self.rawValue | other.rawValue) }
	func        intersection(_ other: Self) -> Self { return Self.init(rawValue: self.rawValue & other.rawValue) }
	func symmetricDifference(_ other: Self) -> Self { return Self.init(rawValue: self.rawValue ^ other.rawValue) }
	
	mutating func               formUnion(_ other: Self) { self = self.union(other) }
	mutating func        formIntersection(_ other: Self) { self = self.intersection(other) }
	mutating func formSymmetricDifference(_ other: Self) { self = self.symmetricDifference(other) }
	
	func contains(_ member: Element) -> Bool {
		let mask = member.rawValue
		return self.rawValue & mask == mask
	}
	
	mutating func insert(_ newMember: Element) -> (inserted: Bool, memberAfterInsert: Element) {
		let inserted = self.contains(newMember)
		self.formUnion(Self(rawValue: newMember.rawValue))
		return (inserted, newMember)
	}
	
	mutating func remove(_ member: Element) -> Element? {
		let alreadyContainsMember = self.contains(member)
		self.subtract(Self(rawValue: member.rawValue))
		return alreadyContainsMember ? member : nil
	}
	
	mutating func update(with newMember: Element) -> Element? {
		let alreadyContainsMember = self.contains(newMember)
		return alreadyContainsMember ? newMember : nil
	}
}
