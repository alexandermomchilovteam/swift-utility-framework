//
//  TappingSequence.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-08-26.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

/// A sequence whose iterator can be "tapped" by a closure
public struct TappingSequence<S: Sequence>: Sequence {
	private let wrapped: S
	private let tappingClosure: (S.Element?) -> Void
	
	public init(wrapping wrapped: S, tappingClosure: @escaping (S.Element?) -> Void) {
		self.wrapped = wrapped
		self.tappingClosure = tappingClosure
	}
	
	public func makeIterator() -> TappingIterator<S.Iterator>{
		return TappingIterator(
			wrapping: self.wrapped.makeIterator(),
			tappingClosure: self.tappingClosure
		)
	}
}

/// An iterator whose `next()` calls are "tapped" by the provided closure
public struct TappingIterator<I: IteratorProtocol>: IteratorProtocol {
	private var wrapped: I
	private let tappingClosure: (I.Element?) -> Void
	
	public init(wrapping wrapped: I, tappingClosure: @escaping (I.Element?) -> Void) {
		self.wrapped = wrapped
		self.tappingClosure = tappingClosure
	}
	
	public mutating func next() -> I.Element? {
		let element = self.wrapped.next()
		self.tappingClosure(element)
		return element
	}
}
