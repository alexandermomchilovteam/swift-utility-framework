//: [Previous](@previous)

struct FredkinConnection {
	let a, b, c: Bool
	
	func splice(_ slicer: (_ a: Bool, _ b: Bool, _ c: Bool) -> (a: Bool, b: Bool, c: Bool)) -> FredkinConnection {
		let (a, b, c) = slicer(self.a, self.b, self.c)
		return FredkinConnection(a: a, b: b, c: c)
	}
	
	func process() -> FredkinConnection {
		return c ? FredkinConnection(a: b, b: a, c: c) : self
	}
}

func fredkinGate<T>(a: T, b: T, c: Bool) -> (a: T, b: T, c: Bool) {
	return c ? (b, a, c) : (a, b, c)
}

func fredkinNot(_ a: Bool) -> Bool {
	return FredkinConnection(a: true, b: false, c: a).process().a
}

func fredkinAND(_ a: Bool, _ b: Bool) -> Bool {
	return FredkinConnection(a: false, b: a, c: b).process().a
}

func fredkinOR(_ a: Bool, _ b: Bool) -> Bool {
	return FredkinConnection(a: a, b: true, c: b).process().a
}

func fredkinXOR(_ a: Bool, _ b: Bool) -> Bool {
	return FredkinConnection(a: false, b: true, c: a)
		.process()
		.splice { o1, o2, c in (o1, o2, b) }
		.process()
		.a
}

func fredkinNAND(_ a: Bool, _ b: Bool) -> Bool {
	return FredkinConnection(a: false, b: true, c: a)
		.process()
		.splice { o1, o2, c in (true, o2, b) }
		.process()
		.a
}

func fredkinNOR(_ a: Bool, _ b: Bool) -> Bool {
	return FredkinConnection(a: true, b: false, c: a)
		.process()
		.splice { o1, o2, c in (o1, false, b) }
		.process()
		.a
}

func fredkinXNOR(_ a: Bool, _ b: Bool) -> Bool {
	return FredkinConnection(a: true, b: false, c: a)
		.process()
		.splice { o1, o2, c in (o1, o2, b) }
		.process()
		.a
}


let (t, f) = (true, false)

func printTruthTable(_ title: String, _ fn: (Bool, Bool) -> Bool) {
	print(title)
	print(fn(f, f))
	print(fn(f, t))
	print(fn(t, f))
	print(fn(t, t))
}

print("NOT:")
print(fredkinNot(t))
print(fredkinNot(f))

printTruthTable("\nAND:", fredkinAND)
printTruthTable("\nOR:", fredkinOR)
printTruthTable("\nXOR:", fredkinXOR)

printTruthTable("\n\n\nNAND:", fredkinNAND)
printTruthTable("\nNOR:", fredkinNOR)
printTruthTable("\nXNOR:", fredkinXNOR)

//: [Next](@next)
