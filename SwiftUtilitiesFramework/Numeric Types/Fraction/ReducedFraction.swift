//
//  ReducedFraction.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public struct ReducedFraction {
	let frac: Fraction
	
	internal init(_ frac: Fraction) {
		self.frac = frac
	}
	
	internal init(reducing frac: Fraction) {
		self.init(numerator: frac.num, denominator: frac.denum)
	}
	
	public init(numerator: Int, denominator: Int) {
		let _gcd = gcd(numerator, denominator)
		self.init(Fraction(numerator: numerator / _gcd, denominator: denominator / _gcd))
	}
}

extension Fraction {
	public var reduced: ReducedFraction { return ReducedFraction(reducing: self) }
}

extension ReducedFraction: CustomDebugStringConvertible {
	public var debugDescription: String { return self.frac.debugDescription }
}
