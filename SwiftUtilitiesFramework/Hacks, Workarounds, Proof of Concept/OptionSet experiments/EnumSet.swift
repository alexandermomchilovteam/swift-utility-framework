//
//  EnumSet.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-21.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

protocol EnumSet: OptionSet where
	Element: RawRepresentable,
	Element.RawValue: BinaryInteger,
	Self.RawValue == Element.RawValue {}

extension EnumSet {
	init(enumCase: Element) { self = Self(rawValue: enumCase.rawValue) }
	
	mutating func insert(_ newMember: Element) -> (inserted: Bool, memberAfterInsert: Element) {
		let inserted = self.contains(newMember)
		self.formUnion(Self(rawValue: newMember.rawValue))
		return (inserted, newMember)
	}
	
	mutating func remove(_ member: Element) -> Element? {
		let alreadyContainsMember = self.contains(member)
		self.subtract(Self(rawValue: member.rawValue))
		return alreadyContainsMember ? member : nil
	}
	
	mutating func update(with newMember: Element) -> Element? {
		let alreadyContainsMember = self.contains(newMember)
		return alreadyContainsMember ? newMember : nil
	}
}


enum Weekday: UInt8, CaseIterable {
	case Sunday = 1
	case Monday = 2
	case Tuesday = 4
	case Wednesday = 8
	case Thursday = 16
	case Friday = 32
	case Saturday = 64
}

struct WeekdaySet: EnumSet, Equatable {
	typealias Element = Weekday
	
	let rawValue: UInt8
	
	init(rawValue: UInt8) {
		self.rawValue = rawValue
	}
	
	static let Everyday: WeekdaySet = [.Sunday, .Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday]
	static let Weekdays: WeekdaySet = [.Monday, .Tuesday, .Wednesday, .Thursday, .Friday]
	static let Weekends: WeekdaySet = [.Saturday, .Sunday]
	
	func contains(_ member: Element) -> Bool {
		print("contains(\(member))")
		let mask = member.rawValue
		return self.rawValue & mask == mask
	}
}

extension WeekdaySet: Sequence {
	func makeIterator() -> Array<Weekday>.Iterator {
		return Weekday.allCases.filter(self.contains).makeIterator()
	}
}

//print(WeekdaySet.Weekends.contains(.Monday))
//print(WeekdaySet.Weekends.contains(.Saturday))
//print(WeekdaySet.Weekends.contains(.Sunday))
//print(Array(WeekdaySet.Everyday).map { "\($0): \($0.rawValue)" })
