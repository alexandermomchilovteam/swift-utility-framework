// https://stackoverflow.com/a/40372665/3141234

import Foundation

enum Weekday: UInt8, CaseIterable {
	case Sunday = 1
	case Monday = 2
	case Tuesday = 4
	case Wednesday = 8
	case Thursday = 16
	case Friday = 32
	case Saturday = 64
}

extension Weekday: CustomStringConvertible {
	static let weekDayNames: [String] = DateFormatter().weekdaySymbols
	
	public var description: String {
		let index = Int(log2(Double(self.rawValue)))
		return Weekday.weekDayNames[index]
	}
}

struct WeekdaySet: OptionSet, Equatable {
	let rawValue: UInt8
	
	init(rawValue: UInt8) {
		self.rawValue = rawValue
	}
	
	init(_ weekday: Weekday) {
		self.init(rawValue: weekday.rawValue)
	}
	
	static let Sunday = WeekdaySet(.Sunday)
	static let Monday = WeekdaySet(.Monday)
	static let Tuesday = WeekdaySet(.Tuesday)
	static let Wednesday = WeekdaySet(.Wednesday)
	static let Thursday = WeekdaySet(.Thursday)
	static let Friday = WeekdaySet(.Friday)
	static let Saturday = WeekdaySet(.Saturday)
	
	static let Everyday: WeekdaySet = [.Sunday, .Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday]
	static let Weekdays: WeekdaySet = [.Monday, .Tuesday, .Wednesday, .Thursday, .Friday]
	static let Weekends: WeekdaySet = [.Saturday, .Sunday]
	
	
	var count: Int { return self.rawValue.nonzeroBitCount }
	
	var isConsecutive: Bool {
		return (self.rawValue.bitWidth
			- self.rawValue.leadingZeroBitCount
			- self.rawValue.trailingZeroBitCount) == self.count
	}
}


extension WeekdaySet: Sequence {
	static let allDays: Array<WeekdaySet> = [.Saturday, .Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Sunday]
	
	@inlinable // generic-performance
	public func contains(_ member: WeekdaySet) -> Bool {
		return self.isSuperset(of: member)
	}
	
	@inlinable // protocol-only
	public func isSuperset(of other: WeekdaySet) -> Bool {
		return other.isSubset(of: self)
	}
	
	func makeIterator() -> Array<WeekdaySet>.Iterator {
		return Weekday.allCases
			.map(WeekdaySet.init)
			.filter(self.contains)
			.makeIterator()
	}
}

extension WeekdaySet: CustomStringConvertible {
	var description: String {
		switch self {
		case []: return "No days"
		case .Everyday: return "Everyday"
		case .Weekdays: return "Weekdays"
		case .Weekends: return "Weekends"
			
			//		case _ where self.isConsecutive(): // Consecutive range of days
			//			let min = array.first!
			//			let max = array.last!
			//			return "\(Weekday(rawValue: min)!) - \(Weekday(rawValue: max)!)"
			
		default:
			print(Array(self));
			return Array(self).description //arbitrary days
		}
	}
}

print(WeekdaySet.Everyday)
print(WeekdaySet.Weekdays)
print(WeekdaySet.Weekends)
let monToThurs: WeekdaySet = [.Monday, .Tuesday, .Wednesday, .Thursday]
print(monToThurs) // => Monday - Thursday
let tuesWedThursSat: WeekdaySet = [.Tuesday, .Wednesday, .Thursday, .Saturday]
print(tuesWedThursSat) // => [Saturday, Wednesday, Thursday, Tuesday]
