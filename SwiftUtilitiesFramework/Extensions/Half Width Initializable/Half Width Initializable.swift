//
//  Half Width Initializable.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public protocol PairedUnsignedInteger: UnsignedInteger {
	associatedtype SignedSelf: PairedSignedInteger
}

public protocol PairedSignedInteger: SignedInteger {
	associatedtype UnsignedSelf: PairedUnsignedInteger
}



public protocol BitPattternInitializable: FixedWidthInteger {
	associatedtype BitPatternSource: FixedWidthInteger
	init(bitPattern: BitPatternSource)
}

public protocol UnsignedBitPattternInitializable: BitPattternInitializable, PairedSignedInteger
where BitPatternSource == UnsignedSelf {}

public protocol SignedBitPattternInitializable: BitPattternInitializable, PairedUnsignedInteger
where BitPatternSource == SignedSelf {}

extension UInt:   PairedUnsignedInteger { public typealias   SignedSelf =  Int   }
extension UInt8:  PairedUnsignedInteger { public typealias   SignedSelf =  Int8  }
extension UInt16: PairedUnsignedInteger { public typealias   SignedSelf =  Int16 }
extension UInt32: PairedUnsignedInteger { public typealias   SignedSelf =  Int32 }
extension UInt64: PairedUnsignedInteger { public typealias   SignedSelf =  Int64 }
extension  Int:     PairedSignedInteger { public typealias UnsignedSelf = UInt   }
extension  Int8:    PairedSignedInteger { public typealias UnsignedSelf = UInt8  }
extension  Int16:   PairedSignedInteger { public typealias UnsignedSelf = UInt16 }
extension  Int32:   PairedSignedInteger { public typealias UnsignedSelf = UInt32 }
extension  Int64:   PairedSignedInteger { public typealias UnsignedSelf = UInt64 }

extension UInt:     SignedBitPattternInitializable {}
extension UInt8:    SignedBitPattternInitializable {}
extension UInt16:   SignedBitPattternInitializable {}
extension UInt32:   SignedBitPattternInitializable {}
extension UInt64:   SignedBitPattternInitializable {}
extension  Int:   UnsignedBitPattternInitializable {}
extension  Int8:  UnsignedBitPattternInitializable {}
extension  Int16: UnsignedBitPattternInitializable {}
extension  Int32: UnsignedBitPattternInitializable {}
extension  Int64: UnsignedBitPattternInitializable {}



public protocol UnsignedHalfWidthInitializable: UnsignedInteger, FixedWidthInteger {
	associatedtype UnsignedHalfWidth: FixedWidthInteger
	init(high: UnsignedHalfWidth, low: UnsignedHalfWidth)
}

public extension UnsignedHalfWidthInitializable {
	init(high: UnsignedHalfWidth, low: UnsignedHalfWidth) {
		self.init(littleEndian: Self(high) << UnsignedHalfWidth.bitWidth | Self(low))
	}
}

public protocol SignedHalfWidthInitializable: UnsignedBitPattternInitializable {
	associatedtype UnsignedHalfWidth: UnsignedInteger, FixedWidthInteger
	init(high: UnsignedHalfWidth, low: UnsignedHalfWidth)
}

public extension SignedHalfWidthInitializable {
	init(high: UnsignedHalfWidth, low: UnsignedHalfWidth) {
		let unsignedPromotedHigh = UnsignedSelf(high)
		let unsignedPromotedLow = UnsignedSelf(low)
		let unsignedLittleEndian = unsignedPromotedHigh << UnsignedHalfWidth.bitWidth | unsignedPromotedLow
		let signedLittleEndian = Self.init(bitPattern: unsignedLittleEndian)
		self.init(littleEndian: signedLittleEndian)
	}
}



extension UInt16: UnsignedHalfWidthInitializable { public typealias UnsignedHalfWidth = UInt8  }
extension UInt32: UnsignedHalfWidthInitializable { public typealias UnsignedHalfWidth = UInt16 }
extension UInt64: UnsignedHalfWidthInitializable { public typealias UnsignedHalfWidth = UInt32 }
extension  Int16:   SignedHalfWidthInitializable { public typealias UnsignedHalfWidth = UInt8  }
extension  Int32:   SignedHalfWidthInitializable { public typealias UnsignedHalfWidth = UInt16 }
extension  Int64:   SignedHalfWidthInitializable { public typealias UnsignedHalfWidth = UInt32 }
