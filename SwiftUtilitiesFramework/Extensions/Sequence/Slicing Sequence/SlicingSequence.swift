//
//  SlicingSequence.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-10.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public protocol SliceSequenceDelegate {
	associatedtype Element

	mutating func initialize(initialElement: Element)
	mutating func belongsInCurrentSlice(_ newElement: Element) -> Bool
}

public struct ClosureBasedState<State, Element>: SliceSequenceDelegate {
	let initializeState: (Element) -> State
	let _belongsInCurrentSlice: (State, Element) -> Bool
	let updateState: (State, Element) -> State

	var state: State!

	public init(
		initializeState: @escaping (Element) -> State,
		belongsInCurrentSlice: @escaping (State, Element) -> Bool,
		updateState: @escaping (State, Element) -> State
		) {
		self.initializeState = initializeState
		self._belongsInCurrentSlice = belongsInCurrentSlice
		self.updateState = updateState
	}

	public mutating func initialize(initialElement: Element) {
		self.state = self.initializeState(initialElement)
	}

	public mutating func belongsInCurrentSlice(_ nextElement: Element) -> Bool {
		defer { self.state = self.updateState(self.state, nextElement) }
		return self._belongsInCurrentSlice(self.state, nextElement)
	}
}



public struct SlicingSequence<Delegate: SliceSequenceDelegate, C: Collection>: Sequence
where Delegate.Element == C.Element {
	private let wrapped: C
	private var delegate: Delegate

	public init(_ wrapped: C, delegate: Delegate) {
		self.wrapped = wrapped
		self.delegate = delegate
	}

	public func makeIterator() -> SlicingIterator<Delegate, C> {
		return SlicingIterator(wrapped, delegate: delegate)
	}
}

public struct SlicingIterator<Delegate: SliceSequenceDelegate, C: Collection>: IteratorProtocol
where Delegate.Element == C.Element {

	private let wrapped: C
	private var delegate: Delegate
	private var wrappedIterator: Zip2Sequence<C.Indices, C>.Iterator

	private var currentRunStartIndex: C.Index? = nil

	internal init(_ wrapped: C, delegate: Delegate) {
		self.wrapped = wrapped
		self.wrappedIterator = zip(wrapped.indices, wrapped).makeIterator()
		self.delegate = delegate
	}

	public mutating func next() -> C.SubSequence? {
		if self.currentRunStartIndex == nil {
			if let (nextOffset, nextElement) = wrappedIterator.next() {
				self.currentRunStartIndex = nextOffset
				delegate.initialize(initialElement: nextElement)
			}
			else { // Sequence is empty, no runs left.
				return nil
			}
		}

		guard let currentRunStartIndex = self.currentRunStartIndex else {
			fatalError("self.currentRunStartIndex should be set if we got to here")
		}

		while let (nextOffset, nextElement) = wrappedIterator.next() {
			if !self.delegate.belongsInCurrentSlice(nextElement) {
				self.currentRunStartIndex = nextOffset // Pick up from here next time
				return self.wrapped[currentRunStartIndex ..< nextOffset] // Yield a slice
			}
		}

		if let currentRunStartIndex = self.currentRunStartIndex {
			// finish up the last run, if the is one
			self.currentRunStartIndex = nil
			return self.wrapped[currentRunStartIndex ..< self.wrapped.endIndex]
		}

		fatalError("should be unreachable")
	}
}
