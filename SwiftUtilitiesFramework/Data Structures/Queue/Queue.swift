//
//  Queue.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-06-10.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

// TODO: Never wraps around
struct Queue<T> {
	var headIndex = 0
	var endIndex = 0
	var elements = [T]()


	func peek() -> T? {
		if !elements.indices.contains(headIndex) { return nil }
		else { return elements[headIndex] }
	}

	mutating func enqueue(_ newElement: T) {
		elements.append(newElement)
		endIndex += 1
	}

	mutating func dequeue() -> T? {
		guard let result = peek() else { return nil }
		headIndex += 1
		return result
	}
}
