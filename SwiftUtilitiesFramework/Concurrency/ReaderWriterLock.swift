//
//  ReaderWriterLock.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Dispatch

// https://en.wikipedia.org/wiki/Readers–writer_lock
class ReaderWriterLock<T> {
	private let queue: DispatchQueue
	private var _value: T
	
	// The queue must be concurrent!
	init(queue: DispatchQueue, initialValue: T) {
		self.queue = queue
		self._value = initialValue
	}
	
	convenience init(queueLabel: String = "ReaderWriterLock<\(T.self)>", initialValue: T) {
		let newQueue = DispatchQueue(label: queueLabel, attributes: .concurrent)
		self.init(queue: newQueue, initialValue: initialValue)
	}
	
	public var value: T {
		get {
			// Getting is done synchronously, and concurrently
			return queue.sync { _value }
		}
		set {
			// Setting is done synchronously, but waits until the queue is clear,
			// and runs exlusively (.barrier)
			queue.sync(flags: .barrier) { self._value = newValue }
		}
	}
}

