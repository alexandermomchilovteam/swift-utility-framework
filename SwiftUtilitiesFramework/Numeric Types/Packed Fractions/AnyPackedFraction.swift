//
//  AnyPackedFraction.swift
//  SwiftUtilities
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public struct AnyPackedFraction<I: BinaryInteger> {
	public var i: I
	public private(set) var numeratorBitWidth: Int // "m"
	public private(set) var denominatorBitWidth: Int { // "n"
		get { return i.bitWidth - numeratorBitWidth }
		set { self.numeratorBitWidth = i.bitWidth - newValue }
	}
	
	
	
	public init(bitPattern: I, numeratorBitWidth: Int, denominatorBitWidth: Int) {
		assert(numeratorBitWidth + denominatorBitWidth == bitPattern.bitWidth, """
			The number of bits in the numerator (\(numeratorBitWidth)) and denominator (\(denominatorBitWidth)) \
			must sum to the total number of bits in the integer \(bitPattern.bitWidth)
			""")
		
		self.i = bitPattern
		self.numeratorBitWidth = numeratorBitWidth
	}
}

extension AnyPackedFraction {
	public var numerator: I {
		return i >> denominatorBitWidth
	}
	
	public var denominator: I {
		if denominatorBitWidth == 0 { return 1 }
		let denominatorMask: I = (1 << I(numeratorBitWidth)) - 1
		return i & denominatorMask
	}
	
	public var ratio: Double { return Double(numerator) / Double(denominator) }
	
	public var qFormatDescription: String {
		let (m, n) = (self.numeratorBitWidth, self.denominatorBitWidth)
		return (n == 0) ?  "Q\(m)" : "Q\(m).\(n)"
	}
	
	//	Might be useful to implement something like this:
	//	init(numerator: I, numeratorBits: Int, denominator: I, denominatorBits: Int) {
	//
	//	}
}
