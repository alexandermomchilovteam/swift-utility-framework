//
//  CollatzSequenceTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import SwiftUtilitiesFramework

class CollatzSequenceTests: XCTestCase {
	
    func testStartAt5() {
        XCTAssertEqual(
			Array(CollatzSequence(startingAt: 5)),
			[5,16,8,4,2,1]
		)
    }
}
