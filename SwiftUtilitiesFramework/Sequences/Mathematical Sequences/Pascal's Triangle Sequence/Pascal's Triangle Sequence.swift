//
//  Pascal's Triangle Sequence.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-08-26.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public struct PascalsTriangleIterator: IteratorProtocol {
	private var lastRow = [Int]()
	
	public init() {}
	
	private mutating func simpleWay() -> [Int]? {
		let middlePairs = zip(self.lastRow, self.lastRow.dropFirst())
		let newRow: [Int] = ([1] + middlePairs.map(+) + [1])
		defer { self.lastRow = newRow }
		return lastRow
	}
	
	private mutating func inPlaceEfficientWay() -> [Int]? {
		for ((offset: index, element: prev), current) in zip(self.lastRow.enumerated().dropFirst(), self.lastRow) {
			self.lastRow[index] = prev + current
		}
		
		self.lastRow.append(1)
		
		return self.lastRow
	}
	
	public mutating func next() -> [Int]? {
		return inPlaceEfficientWay()
	}
}

public struct PascalsTriangleSequence: Sequence {
	public init() {}
	
	public func makeIterator() -> PascalsTriangleIterator {
		return PascalsTriangleIterator()
	}
}
