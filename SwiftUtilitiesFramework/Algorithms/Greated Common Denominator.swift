//
//  Greated Common Denominator.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public func gcd<Number: BinaryInteger>(_ lhs: Number, _ rhs: Number) -> Number {
	var (lhs, rhs) = (lhs, rhs) // make mutable copies
	
	while rhs != (0 as Number) {
		(lhs, rhs) = (rhs, lhs % rhs)
	}
	
	return lhs
}
