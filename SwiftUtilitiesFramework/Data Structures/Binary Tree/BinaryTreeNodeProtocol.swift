//
//  BinaryTreeNodeProtocol
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-30.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public protocol BinaryTreeNodeProtocol: AnyObject {
	associatedtype Payload
	var payload: Payload { get }
	var left: Self? { get }
	var right: Self? { get }
}
