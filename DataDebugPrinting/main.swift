//
//  main.swift
//  DataDebugPrinting
//
//  Created by Alexander Momchilov on 2019-06-05.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

let exampleString = "abcdefghijklmnopqrstuvwxyz"
let exampleData = exampleString.data(using: .utf8)!
//print(exampleData.debugBinaryString)

let printer = DataDebugPrinter()
print(printer.debugString(for: exampleData))
