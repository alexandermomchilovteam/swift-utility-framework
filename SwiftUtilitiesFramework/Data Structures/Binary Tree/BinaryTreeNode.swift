//
//  BinaryTreeNode.swift
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-30.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

public final class BinaryTreeNode<T>: BinaryTreeNodeProtocol {
	public let payload: T
	public let left: BinaryTreeNode<T>?
	public let right: BinaryTreeNode<T>?
	
	init(
		_ payload: T,
		left: BinaryTreeNode<T>? = nil,
		right: BinaryTreeNode<T>? = nil
	) {
		self.payload = payload
		self.left = left
		self.right = right
	}
}

extension BinaryTreeNode: CustomStringConvertible {
	public var description: String {
		return "init(payload: \(payload))"
	}
}
