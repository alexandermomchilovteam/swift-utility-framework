//
//  BitVectorGutsTests.swift
//  SwiftUtilitiesFrameworkTests
//
//  Created by Alexander Momchilov on 2019-05-26.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest

func concisePrint(_ bitVector: BitVectorGuts) {
	print(bitVector.map { $0 ? "1" : "0" }.joined(separator: ""))
}

func assert(vector: BitVectorGuts, isEqualTo reference: [Bool]) {
	for (i, (b1, b2)) in zip(vector, reference).enumerated() {
		XCTAssertEqual(b1, b2, "Found \(b1) at index \(i), but expected \(b2).")
	}
}

class BitVectorGutsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testBitVectorGrowth() {
		var bitVector = BitVectorGuts()
		var bools = [Bool]()
		
		for _ in 0..<10000 {
			let bool = Bool.random()
			bitVector.append(bool)
			bools.append(bool)
			
//			concisePrint(bitVector)
			
			assert(vector: bitVector, isEqualTo: bools)
		}
		
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
