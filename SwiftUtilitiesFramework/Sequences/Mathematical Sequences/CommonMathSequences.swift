let fib = sequence(state: (0, 1), next: { (state) -> Int in
	state = (state.1, state.0 + state.1)
	return state.0
})

let fib0 = sequence(state: (0, 1), next: { (state) -> Int in
	defer { state = (state.1, state.0 + state.1) }
	return state.0
})

let exponentsOfTwo = sequence(first: 1, next: {$0 * 2})

let factorials = sequence(state: (1, 1), next: { (state) -> Int in
	state = (state.0 + 1, state.1 * state.0)
	return state.1
})

let perfectSquares = sequence(state: 0, next: { (state) -> Int in
	state += 1
	return state * state
})

let perfectCubes = sequence(state: 0, next: { (state) -> Int in
	state += 1
	return state * state * state
})
