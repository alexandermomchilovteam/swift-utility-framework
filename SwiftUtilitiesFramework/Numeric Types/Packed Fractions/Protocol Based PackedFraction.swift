//
//  Protocol Based PackedFraction.swift
//  SwiftUtilities
//
//  Created by Alexander Momchilov on 2019-05-20.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public protocol PackedFraction {
	associatedtype I: BinaryInteger
	
	var i: I { get set }
	static var numeratorBitWidth: Int { get } // "m"
	static var denominatorBitWidth: Int { get }  // "n"
}

extension PackedFraction {
	
	public static var denominatorBitWidth: Int { return I().bitWidth - Self.numeratorBitWidth }
	
	public static var qFormatDescription: String {
		let (m, n) = (self.numeratorBitWidth, self.denominatorBitWidth)
		return (n == 0) ?  "Q\(m)" : "Q\(m).\(n)"
	}
	
	public var numerator: I {
		return i >> Self.denominatorBitWidth
	}
	
	public var denominator: I {
		if Self.denominatorBitWidth == 0 { return 1 }
		let denominatorMask: I = (1 << I(Self.numeratorBitWidth)) - 1
		return i & denominatorMask
	}
	
	public var ratio: Double { return Double(numerator) / Double(denominator) }
	
	func typeErase() -> AnyPackedFraction<I> {
		return AnyPackedFraction<I>(
			bitPattern: self.i,
			numeratorBitWidth: Self.numeratorBitWidth,
			denominatorBitWidth: Self.denominatorBitWidth
		)
	}
}
