import SwiftUtilitiesFramework



struct EqualElementGroupingDelegate<Element: Equatable>: SliceSequenceDelegate {
	var element: Element! = nil

	init() { }

	mutating func initialize(initialElement: Element) {
		self.element = initialElement
	}

	mutating func belongsInCurrentSlice(_ newElement: Element) -> Bool {
		return self.element == newElement
	}
}

public struct ConsequtiveElementSlicer<Element: Strideable>: SliceSequenceDelegate {
	var lastElement: Element! = nil

	init() { }

	public mutating func initialize(initialElement: Element) {
		self.lastElement = initialElement
	}

	public mutating func belongsInCurrentSlice(_ newElement: Element) -> Bool {
		defer { self.lastElement = newElement }
		return self.lastElement.distance(to: newElement) == +1
	}
}

let ConsequtiveElementSlicer2 = ClosureBasedState<Int, Int>(
	initializeState: { $0 },
	belongsInCurrentSlice: { $0.distance(to: $1) == +1 },
	updateState: { oldState, newElement in newElement }
)
print(Array(SlicingSequence("abbcccdde", delegate: EqualElementGroupingDelegate())))
print(Array(SlicingSequence([1, 2, 3, 15, 16, 17, 18, 19, 20], delegate: EqualElementGroupingDelegate())))
print(Array(SlicingSequence([1, 2, 3, 15, 16, 17, 18, 19, 20], delegate: ConsequtiveElementSlicer())))
print(Array(SlicingSequence([1, 2, 3, 15, 16, 17, 18, 19, 20], delegate: ConsequtiveElementSlicer2)))
