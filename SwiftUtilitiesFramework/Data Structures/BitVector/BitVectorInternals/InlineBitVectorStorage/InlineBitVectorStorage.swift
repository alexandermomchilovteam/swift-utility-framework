//
//  InlineBitVectorStorage
//  SwiftUtilitiesFramework
//
//  Created by Alexander Momchilov on 2019-05-28.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

// Bit    | Usage
// -------+--------------------
// bit 64 | element 0
// bit 63 | element 1
// bit 62 | element 2
// bit 61 | element 3
// bit 60 | element 4
// bit 59 | element 5
// bit 58 | element 6
// bit 57 | element 7
// ...
// bit  7 | element 0
// bit  6 | count bit 6 (32s unit)
// bit  5 | count bit 5 (16s unit)
// bit  4 | count bit 4 ( 8s unit)
// bit  3 | count bit 3 ( 4s unit)
// bit  2 | count bit 2 ( 2s unit)
// bit  1 | count bit 1 ( 1s unit)
// bit  0 | descriminant (0 = reference, 1 = inline)
struct InlineBitVectorStorage: BitVectorStorage {
	enum BitMask { // name space for bit masks
		static let bitStorage  : UInt64 = 0b11111111_11111111_11111111_11111111_11111111_11111111_11111111_10000000
		static let count       : UInt64 = 0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_01111110
		static let discriminant: UInt64 = 0b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000001
	}
	
	var bits: UInt64
	
	init() { self.bits = 0 }
	
	var count: Int {
		get { return Int((self.bits & BitMask.count) >> 1)}
		set {
			assert((0..<self.capacity).contains(newValue), "InlineBitVectorStorage can only contain 56 elements!")
			self.bits = (self.bits & ~BitMask.bitStorage) | (UInt64(newValue << 1) & BitMask.count)
		}
	}
	
	var capacity: Int { return 56 }
	
	
	func bitIndex(for index: Int) -> Int {
		assert((0..<self.count).contains(index), "Index out of bounds")
		return 63 - index
	}
	
	func bitIndex(for index: BitVector.Index) -> Int {
		assert(index.quadwordIndex == 0, "InlineBitVectorStorage only contains one quadword")
		return 63 - index.bitIndex
	}
	
	subscript(index: Int) -> Bool {
		get { return self.bits[bitIndex: bitIndex(for: index)] }
		set { self.bits[bitIndex: bitIndex(for: index)] = newValue }
	}
	
	subscript(index: BitVector.Index) -> Bool {
		get { return self[bitIndex(for: index)] }
		set { self[bitIndex(for: index)] = newValue }
	}
}


extension InlineBitVectorStorage {
	
}
